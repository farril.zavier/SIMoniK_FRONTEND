import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Tugas from "./Tugas/Tugas";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

import { api } from "../../../api";

import classes from "./DaftarTugas.module.css";

class DaftarTugas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            idDeadlineFo: props.match.params.id,
            deadlineFo: {},
            filter: 0,
            row: 0,
        };
    }

    componentDidMount() {
        // this.loadDepos();
        this.loadDeadlineFo();
    }

    loadDeadlineFo = async () => {
        const idDeadlineFo = this.state.idDeadlineFo;
        const response = await api.get("upload-so/deadline-fo/" + idDeadlineFo + "/");
        const fetchedDeadlineFo = response.data;
        this.setState({
            deadlineFo: fetchedDeadlineFo,
        });
    };

    // loadDepos = async () => {
    //     const fetchedDepos = [];
    //     const response = await api.get("upload-so/row/");
    //     for (let key in response.data) {
    //         fetchedDepos.push({
    //             ...response.data[key],
    //         });
    //     }
    //     this.setState({
    //         depos: fetchedDepos,
    //     });
    // };

    filterHandler = (event) => {
        const { name, value } = event.target;
        console.log(name + value);
        this.setState({
            [name]: value,
        });
    };

    submitHandler = (event) => {
        event.preventDefault();
        this.setState({ filter: this.state.row });
    };

    render() {
        let renderDepos;
        if (this.state.deadlineFo.deadlinefodepo_set !== undefined) {
            renderDepos = this.state.deadlineFo.deadlinefodepo_set
                .filter(
                    (row, idx) =>
                        this.state.filter == 0 || row.depo.id.toString() == this.state.filter
                )
                .map((row, idx) => {
                    let status;
                    if (row.depo.is_assigned_deadline) {
                        status = "BELUM SELESAI";
                    } else {
                        status = "SUDAH SELESAI";
                    }
                    return (
                        <Tugas
                            key={idx}
                            idx={idx + 1}
                            nama={row.depo.nama}
                            kodeToko={row.depo.kode_toko}
                            statusStockOpname={status}
                            idDeadlineFo={this.state.idDeadlineFo}
                            idDepo={row.depo.id}
                        />
                    );
                });
        }

        let sortedDeadlinesByDepo, renderDepoFilter;
        if (this.state.deadlineFo.deadlinefodepo_set !== undefined) {
            sortedDeadlinesByDepo = [...this.state.deadlineFo.deadlinefodepo_set];
            sortedDeadlinesByDepo.sort((a, b) =>
                a.depo.nama > b.depo.nama ? 1 : b.depo.nama > a.depo.nama ? -1 : 0
            );
            renderDepoFilter = sortedDeadlinesByDepo.map((row, idx) => {
                return (
                    <option key={idx} value={row.depo.id}>
                        {row.depo.nama}
                    </option>
                );
            });
        }
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/daftar-deadline">Deadline</Breadcrumb.Item>
                        <Breadcrumb.Item active>Tugas</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <h5>Penugasan</h5>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <h6>Cari Berdasarkan</h6>
                                <Form inline onSubmit={this.submitHandler}>
                                    <Form.Control
                                        onChange={this.filterHandler}
                                        name="row"
                                        as="select"
                                    >
                                        <option selected value={0}>
                                            -- Semua Depo --
                                        </option>
                                        {renderDepoFilter}
                                    </Form.Control>
                                    <Button className="ml-3" variant="primary" type="submit">
                                        Cari
                                    </Button>
                                </Form>

                                <br></br>
                                <h5> Semua Depo</h5>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Depo</th>
                                            <th>Kode Depo</th>
                                            <th>Status Stock Opname</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderDepos}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarTugas;
