import React from "react";
import Button from "react-bootstrap/Button";

import classes from "./Tugas.module.css";

const Tugas = (props) => {
    const statuses = [
        { status: "BELUM SELESAI", color: "red" },
        { status: "SUDAH SELESAI", color: "green" },
    ];
    const foundStatus = statuses.find((status) => {
        return props.statusStockOpname === status.status;
    });
    let button;
    if (foundStatus.status === "BELUM SELESAI") {
        button = (
            <Button
                href={"/deadline/detail/" + props.idDeadlineFo + "/unggah-so/" + props.idDepo}
                variant="primary"
            >
                Unggah Stock Opname
            </Button>
        );
    }
    return (
        <tr>
            <td>{props.idx}</td>
            <td>{props.nama}</td>
            <td>{props.kodeToko}</td>
            <td>
                <font color={foundStatus.color}>{foundStatus.status}</font>
            </td>
            <td>{button}</td>
        </tr>
    );
};

export default Tugas;
