import React from "react";
import Button from "react-bootstrap/Button";

import classes from "./Deadline.module.css";

const Deadline = (props) => {
    return (
        <tr>
            <td>{props.idx}</td>
            <td>{new Date(props.tenggatWaktu).toLocaleString()}</td>
            <td>
                {props.status === 0 ? (
                    <span style={{ color: "red" }}>DALAM PROGRES</span>
                ) : (
                    <span style={{ color: "green" }}>TELAH SELESAI</span>
                )}
            </td>
            <td>
                <Button href={"/deadline/detail/" + props.id} variant="primary">
                    Lihat Detail
                </Button>
            </td>
        </tr>
    );
};

export default Deadline;
