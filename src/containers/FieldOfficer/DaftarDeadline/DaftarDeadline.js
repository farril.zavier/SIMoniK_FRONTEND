import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Deadline from "./Deadline/Deadline";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";

import { api } from "../../../api";

import classes from "./DaftarDeadline.module.css";

class DaftarDeadline extends Component {
    constructor(props) {
        super(props);

        this.state = {
            deadlines: [],
        };
    }

    componentDidMount() {
        this.loadDeadlines();
    }

    loadDeadlines = async () => {
        const fetchedDeadlines = [];
        const response = await api.get("upload-so/deadlines/");
        for (let key in response.data) {
            fetchedDeadlines.push({
                ...response.data[key],
            });
        }
        fetchedDeadlines.sort((a, b) => {
            a = new Date(a.tenggat_waktu);
            b = new Date(b.tenggat_waktu);
            return a > b ? -1 : a < b ? 1 : 0;
        });
        this.setState({
            deadlines: fetchedDeadlines,
        });
    };

    render() {
        const renderDeadlines = this.state.deadlines.map((deadline, idx) => {
            return (
                <Deadline
                    key={idx}
                    idx={idx + 1}
                    status={deadline.status}
                    tenggatWaktu={deadline.tenggat_waktu}
                    id={deadline.id}
                />
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Deadline</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <h5>Tenggat Waktu</h5>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <h5> Semua Depo</h5>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tenggat Waktu</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderDeadlines}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarDeadline;
