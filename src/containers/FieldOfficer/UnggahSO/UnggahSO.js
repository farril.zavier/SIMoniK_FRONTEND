import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import { api } from "../../../api";

import classes from "./UnggahSO.module.css";

class UnggahSO extends Component {
    constructor(props) {
        super(props);
        this.fileInput = React.createRef();

        this.state = {
            fileName: "Pilih dokumen",
            successModalShow: false,
            failModalShow: false,
            fieldOfficers: [],
            idDeadlineFo: props.match.params.idDeadlineFo,
            idDepo: props.match.params.idDepo,
            deadline: {},
            deadlineString: "",
            fieldOfficer: "0",
        };
    }

    componentDidMount() {
        this.loadFieldOfficers();
        this.loadDeadlineFo();
    }

    loadFieldOfficers = async () => {
        const fetchedFieldOfficers = [];
        const response = await api.get("upload-so/field-officers/");
        for (let key in response.data) {
            fetchedFieldOfficers.push({
                ...response.data[key],
            });
        }
        fetchedFieldOfficers.sort((a, b) => (a.nama > b.nama ? 1 : b.nama > a.nama ? -1 : 0));
        this.setState({
            fieldOfficers: fetchedFieldOfficers,
        });
    };

    loadDeadlineFo = async () => {
        const idDeadlineFo = this.state.idDeadlineFo;
        const response = await api.get("upload-so/deadlines/" + idDeadlineFo + "/");
        const deadline = response.data;
        this.setState({
            deadline: deadline,
            deadlineString: new Date(deadline.tenggat_waktu).toLocaleString(),
        });
    };

    fileChosenHandler = () => {
        var fileName = this.fileInput.current.files[0].name;
        this.setState({
            fileName: fileName,
        });
    };

    successModalCloseHandler = () => {
        const idDeadlineFo = this.state.idDeadlineFo;
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.replace("/deadline/detail/" + idDeadlineFo + "/");
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    changeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    submitSOHandler = (event) => {
        event.preventDefault();
        this.addSO();
    };

    async addSO() {
        let formData = new FormData();
        var stockOpname = this.fileInput.current.files[0];
        formData.append("file", stockOpname, stockOpname.name);
        formData.append("fieldOfficer", this.state.fieldOfficer);
        formData.append("idDeadlineFo", this.state.idDeadlineFo);
        formData.append("submittedAt", new Date().toUTCString());
        formData.append("idDepo", this.state.idDepo);
        try {
            const response = await api.postStockOpname(formData);
            if (!response.status === 201) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    render() {
        // NOT DONE YETTTTTTTTTTT
        let overdue;
        var deadline = new Date(this.state.deadline.tenggat_waktu);
        var currentDateTime = new Date();
        if (deadline < currentDateTime) {
            overdue = <font color="red">* UNGGAHAN TERLAMBAT</font>;
        }
        const renderFieldOfficers = this.state.fieldOfficers.map((fieldOfficer, idx) => {
            return (
                <option key={idx} value={fieldOfficer.id}>
                    {fieldOfficer.nama}
                </option>
            );
        });
        const deadlineString = this.state.deadlineString;
        return (
            <React.Fragment>
                <Content className={classes.content}>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/daftar-deadline">Deadline</Breadcrumb.Item>
                        <Breadcrumb.Item href={"/deadline/detail/" + this.state.idDeadlineFo}>
                            Tugas
                        </Breadcrumb.Item>
                        <Breadcrumb.Item active>Unggah Stock Opname</Breadcrumb.Item>
                    </Breadcrumb>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Stock opname berhasil diunggah!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Stock opname gagal diunggah. Pastikan format dokumen telah sesuai
                                dengan contoh dan terdapat koneksi internet yang stabil.
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Card>
                        <Card.Body>
                            <Card.Title className={classes.title}>Stock Opname</Card.Title>
                        </Card.Body>
                    </Card>

                    <Card>
                        <Card.Body>
                            <h4>Unggah Stock Opname</h4>
                            <div className={classes.divider}></div>
                            <h5>Stock Opname Januari</h5>
                            <Form onSubmit={this.submitSOHandler}>
                                <Form.Group as={Row} controlId="formTenggatWaktu">
                                    <Form.Label column sm="2">
                                        Tenggat Waktu
                                    </Form.Label>
                                    <Col sm="10">
                                        <Form.Control
                                            plaintext
                                            readOnly
                                            defaultValue={deadlineString}
                                        />
                                        {overdue}
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formDokumen">
                                    <Form.Label column sm="2">
                                        Dokumen
                                    </Form.Label>
                                    <Col sm="10">
                                        <InputGroup className="custom-file mb-2">
                                            <Form.Control
                                                type="file"
                                                className="custom-file-input"
                                                id="customFile"
                                                ref={this.fileInput}
                                                onChange={this.fileChosenHandler}
                                            />
                                            <Form.Label
                                                className="custom-file-label"
                                                for="customFile"
                                            >
                                                {this.state.fileName}
                                            </Form.Label>
                                        </InputGroup>
                                    </Col>
                                    <Col sm="2"></Col>
                                    <Col sm="10">
                                        <Alert variant="warning">
                                            Pastikan dokumen yang diunggah telah sesuai dengan
                                            format.
                                        </Alert>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formRekanFO">
                                    <Form.Label column sm="2">
                                        Rekan Field Officer
                                    </Form.Label>
                                    <Col sm="10">
                                        <Form.Control
                                            as="select"
                                            name="fieldOfficer"
                                            onChange={this.changeHandler}
                                        >
                                            <option selected>-- Tidak Ada --</option>
                                            {renderFieldOfficers}
                                        </Form.Control>
                                    </Col>
                                </Form.Group>

                                <Row>
                                    <Col sm="2"></Col>
                                    <Col sm="10">
                                        <Button variant="success" type="submit">
                                            Submit
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                        </Card.Body>
                    </Card>

                    <Card>
                        <Card.Body>
                            <div className="d-flex justify-content-between">
                                <h4>Dokumen Stock Opname</h4>
                                <Button variant="info">Unduh Template</Button>
                            </div>
                            <div className={classes.divider}></div>
                            <img
                                className={classes.contohSO}
                                src={require("../../../assets/contoh_so.png")}
                            />
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default UnggahSO;
