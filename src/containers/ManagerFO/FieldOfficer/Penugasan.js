import React from "react";
import Button from "react-bootstrap/Button";

const Penugasan = (props) => {
    return (
        <tr>
            <td>{props.idx}</td>
            <td>{props.waktuRilis}</td>
            <td>{props.deadline}</td>
            <td>
                {props.status === 0 ? (
                    <span style={{ color: "red" }}>DALAM PROGRES</span>
                ) : (
                    <span style={{ color: "green" }}>TELAH SELESAI</span>
                )}
            </td>
            <td>{props.fieldOfficer}</td>
            <td>
                {props.status === 0 ? (
                    <Button onClick={props.edit} variant="outline-primary">
                        Edit
                    </Button>
                ) : (
                    <React.Fragment></React.Fragment>
                )}
            </td>
            <td>
                {props.status === 0 ? (
                    <Button onClick={props.finish} variant="warning">
                        Selesaikan
                    </Button>
                ) : (
                    <React.Fragment></React.Fragment>
                )}
            </td>
        </tr>
    );
};

export default Penugasan;
