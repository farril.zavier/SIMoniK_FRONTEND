import React from "react";
import Form from "react-bootstrap/Form";

import classes from "./DepoCheckbox.module.css";

const DepoCheckbox = (props) => {
    return (
        <Form.Check
            inline
            checked={props.checked}
            type="checkbox"
            onChange={props.onChange}
            id={`depo-${props.id}`}
            label={props.nama}
        />
    );
};

export default DepoCheckbox;
