import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import DateTimePicker from "react-datetime-picker";
import Modal from "react-bootstrap/Modal";
import DepoCheckbox from "./DepoCheckbox";
import { api } from "../../../api";

import classes from "./JadwalPenugasan.module.css";

class JadwalPenugasan extends Component {
    constructor(props) {
        super(props);

        this.state = {
            successModalShow: false,
            failModalShow: false,
            fieldOfficers: [],
            fieldOfficer: "0",
            waktuRilis: new Date(),
            tenggatWaktu: new Date(),
            currentManajer: undefined,
            isChoosingDepos: false,
            depoIds: [],
        };
    }

    componentDidMount() {
        this.loadFieldOfficers();
    }

    loadFieldOfficers = async () => {
        const fetchedFieldOfficers = [];
        const response = await api.get("assignments/field-officers/");
        for (let key in response.data) {
            fetchedFieldOfficers.push({
                ...response.data[key],
            });
        }
        this.setState({
            fieldOfficers: fetchedFieldOfficers,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.reload();
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    chooseDepoModalCloseHandler = () => {
        this.setState({
            isChoosingDepos: false,
        });
    };

    changeHandler = (event) => {
        const { name, value } = event.target;
        if (name === "fieldOfficer") {
            var foundFo = this.state.fieldOfficers.find(
                (fieldOfficer) => fieldOfficer.id === Number(value)
            );
            const managedDepoIds = [];
            for (let depo of foundFo.depo_set) {
                if (!depo.is_assigned_deadline) {
                    managedDepoIds.push({ id: depo.id, checked: true });
                }
            }
            this.setState({
                depoIds: managedDepoIds,
            });
        }
        this.setState({
            [name]: value,
        });
    };

    waktuRilisChangeHandler = (waktuRilis) => {
        this.setState({
            waktuRilis,
        });
    };

    tenggatWaktuChangeHandler = (tenggatWaktu) => {
        this.setState({
            tenggatWaktu,
        });
    };

    submitDeadlineFOHandler = (event) => {
        event.preventDefault();
        this.addDeadlineFO();
    };

    chooseDepoHandler = (event) => {
        event.preventDefault();
        this.setState({
            isChoosingDepos: true,
        });
    };

    checkboxChangeHandler = (id) => {
        const { depoIds } = this.state;
        const elementExists = depoIds.find((element) => element.id === id) !== undefined;
        if (elementExists) {
            const idx = depoIds.findIndex((element) => element.id === id);
            if (idx > -1) {
                depoIds[idx].checked = !depoIds[idx].checked;
            }
        } else {
            console.log("Element doesn't exist");
        }
        this.setState({
            depoIds: depoIds,
        });
    };

    async addDeadlineFO() {
        const listIdDepo = [];
        for (let element of this.state.depoIds) {
            if (element.checked) {
                listIdDepo.push(element.id);
            }
        }
        if (listIdDepo.length === 0) {
            throw Error("No depo chosen.");
        }
        const deadlineFO = {
            waktuRilis: this.state.waktuRilis.toUTCString(),
            tenggatWaktu: this.state.tenggatWaktu.toUTCString(),
            idFieldOfficer: this.state.fieldOfficer,
            listIdDepo: listIdDepo,
        };
        try {
            const response = await api.post("assignments/deadline-fo/", deadlineFO);
            if (!response.status === 201) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    render() {
        const renderFieldOfficers = this.state.fieldOfficers.map((fieldOfficer, idx) => {
            return (
                <option key={idx} value={fieldOfficer.id}>
                    {fieldOfficer.nama}
                </option>
            );
        });

        let renderPilihDepoButton;
        if (this.state.fieldOfficer !== undefined && this.state.fieldOfficer !== "0") {
            renderPilihDepoButton = (
                <Form.Group as={Row} controlId="buttonPilihDepo">
                    <Form.Label column sm={2}></Form.Label>
                    <Col sm={6}>
                        <Button onClick={this.chooseDepoHandler} variant="secondary">
                            Pilih Depo
                        </Button>
                    </Col>
                </Form.Group>
            );
        }

        let renderDaftarDepo;
        if (this.state.fieldOfficer !== undefined) {
            let fieldOfficer = this.state.fieldOfficers.find(
                (e) => e.id === Number(this.state.fieldOfficer)
            );
            if (fieldOfficer !== undefined) {
                renderDaftarDepo = fieldOfficer.depo_set.map((depo, idx) => {
                    if (!depo.is_assigned_deadline) {
                        return (
                            <DepoCheckbox
                                checked={
                                    this.state.depoIds.find((element) => element.id === depo.id)
                                        .checked
                                }
                                onChange={() => this.checkboxChangeHandler(depo.id)}
                                id={depo.id}
                                nama={depo.nama}
                            />
                        );
                    }
                });
            }
        }

        let renderChosenDepos;
        if (this.state.fieldOfficer !== undefined) {
            let fieldOfficer = this.state.fieldOfficers.find(
                (e) => e.id === Number(this.state.fieldOfficer)
            );
            if (fieldOfficer !== undefined) {
                renderChosenDepos = fieldOfficer.depo_set.map((depo, idx) => {
                    if (!depo.is_assigned_deadline) {
                        const element = this.state.depoIds.find(
                            (element) => element.id === depo.id
                        );
                        if (element !== undefined && element.checked) {
                            return (
                                <Col sm="3">
                                    <Form.Control
                                        plaintext
                                        readOnly
                                        defaultValue={depo.nama}
                                        className={"text-secondary " + classes.ellipsis}
                                    />
                                </Col>
                            );
                        }
                    }
                });
            }
        }
        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Penugasan berhasil dibuat!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Penugasan gagal dibuat!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.isChoosingDepos}
                        onHide={this.chooseDepoModalCloseHandler}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Pilih Depo</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>{renderDaftarDepo}</Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Tambah Penugasan</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Rencana Penugasan</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <br></br>
                                <Form onSubmit={this.submitDeadlineFOHandler}>
                                    <Form.Group as={Row} controlId="inputWaktuRilis">
                                        <Form.Label column sm={2}>
                                            Waktu Rilis
                                        </Form.Label>

                                        <Col sm={6}>
                                            <DateTimePicker
                                                value={this.state.waktuRilis}
                                                onChange={this.waktuRilisChangeHandler}
                                            ></DateTimePicker>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="inputTenggatWaktu">
                                        <Form.Label column sm={2}>
                                            Tenggat Waktu
                                        </Form.Label>

                                        <Col sm={6}>
                                            <DateTimePicker
                                                value={this.state.tenggatWaktu}
                                                onChange={this.tenggatWaktuChangeHandler}
                                            ></DateTimePicker>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="inputFieldOfficer">
                                        <Form.Label column sm={2}>
                                            Field Officer
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                as="select"
                                                name="fieldOfficer"
                                                value={this.state.fieldOfficer}
                                                onChange={this.changeHandler}
                                            >
                                                <option value={"0"}>Pilih nama...</option>
                                                {renderFieldOfficers}
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>

                                    {renderPilihDepoButton}

                                    <Form.Group as={Row} controlId="daftarDepo">
                                        <Form.Label column sm={2}>
                                            Daftar Depo
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Row>{renderChosenDepos}</Row>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={{ span: 10, offset: 2 }}>
                                            <Button variant="primary" type="submit">
                                                Kirim
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default JadwalPenugasan;
