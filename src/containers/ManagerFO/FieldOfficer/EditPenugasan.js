import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import DateTimePicker from "react-datetime-picker";
import Modal from "react-bootstrap/Modal";
import { api } from "../../../api";

import classes from "./EditPenugasan.module.css";

class EditPenugasan extends Component {
    constructor(props) {
        super(props);

        this.state = {
            successModalShow: false,
            failModalShow: false,
            idPenugasan: props.match.params.id,
            fieldOfficers: [],
            penugasan: {},
            fieldOfficer: "0",
            tenggatWaktu: new Date(),
            currentManajer: undefined,
            deadlines: [],
        };
    }

    componentDidMount() {
        this.loadPenugasan();
        this.loadFieldOfficers();
    }

    loadPenugasan = async () => {
        let idPenugasan = this.state.idPenugasan;
        const response = await api.get("assignments/deadlines/" + idPenugasan + "/");
        const penugasan = response.data;
        this.setState({
            penugasan: penugasan,
            tenggatWaktu: new Date(penugasan.deadline.tenggat_waktu),
            fieldOfficer: penugasan.field_officer.id.toString(),
        });
    };

    loadFieldOfficers = async () => {
        const fetchedFieldOfficers = [];
        const response = await api.get("assignments/field-officers/");
        for (let key in response.data) {
            fetchedFieldOfficers.push({
                ...response.data[key],
            });
        }
        this.setState({
            fieldOfficers: fetchedFieldOfficers,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.reload();
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    submitDeadlineFOHandler = (event) => {
        event.preventDefault();
        this.editDeadlineFO();
    };

    async editDeadlineFO() {
        const editedDeadline = {
            tenggat_waktu: this.state.tenggatWaktu,
        };
        let idDeadline = this.state.penugasan.deadline.id;
        const response = await api.put(
            "assignments/edit-tenggat-waktu/" + idDeadline + "/",
            editedDeadline
        );
        const deadline = response.data;

        const deadlineFO = {
            field_officer: this.state.fieldOfficer,
            deadline: deadline.id,
        };
        try {
            let idPenugasan = this.state.idPenugasan;
            const response = await api.put(
                "assignments/edit-deadline/" + idPenugasan + "/",
                deadlineFO
            );
            if (!response.status === 200) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    changeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    dateTimeChangeHandler = (tenggatWaktu) => {
        this.setState({
            tenggatWaktu,
        });
    };

    render() {
        const renderFieldOfficers = this.state.fieldOfficers.map((fieldOfficer, idx) => {
            return (
                <option key={idx} value={fieldOfficer.id}>
                    {fieldOfficer.nama}
                </option>
            );
        });

        let renderDaftarDepo;
        if (this.state.fieldOfficer !== undefined) {
            let fieldOfficer = this.state.fieldOfficers.find(
                (e) => e.id === Number(this.state.fieldOfficer)
            );
            if (fieldOfficer !== undefined) {
                renderDaftarDepo = fieldOfficer.depo_set.map((depo, idx) => {
                    return (
                        <Form.Control
                            plaintext
                            readOnly
                            defaultValue={depo.nama}
                            className="text-secondary"
                        />
                    );
                });
            }
        }
        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Penugasan berhasil diubah!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Penugasan gagal diubah!
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/penugasan">Daftar Penugasan</Breadcrumb.Item>
                        <Breadcrumb.Item active>Ubah Detail Penugasan</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Rencana Penugasan</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <br></br>
                                <Form onSubmit={this.submitDeadlineFOHandler}>
                                    <Form.Group as={Row} controlId="inputTenggatWaktu">
                                        <Form.Label column sm={2}>
                                            Tenggat Waktu
                                        </Form.Label>

                                        <Col sm={6}>
                                            <DateTimePicker
                                                value={this.state.tenggatWaktu}
                                                onChange={this.dateTimeChangeHandler}
                                            ></DateTimePicker>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="inputFieldOfficer">
                                        <Form.Label column sm={2}>
                                            Field Officer
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                as="select"
                                                name="fieldOfficer"
                                                value={this.state.fieldOfficer}
                                                onChange={this.changeHandler}
                                            >
                                                <option value={"0"}>Pilih nama...</option>
                                                {renderFieldOfficers}
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="daftarDepo">
                                        <Form.Label column sm={2}>
                                            Daftar Depo
                                        </Form.Label>
                                        <Col sm={6}>{renderDaftarDepo}</Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Col sm={{ span: 10, offset: 2 }}>
                                            <Button variant="primary" type="submit">
                                                Kirim
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default EditPenugasan;
