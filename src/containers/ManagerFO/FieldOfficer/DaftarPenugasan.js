import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import { api } from "../../../api";
import Penugasan from "./Penugasan";

import classes from "./DaftarPenugasan.module.css";

class DaftarPenugasan extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalShow: false,
            successModalShow: false,
            failModalShow: false,
            daftarPenugasan: [],
            currentId: undefined,
        };
    }

    modalShowHandler(penugasanId) {
        this.setState({
            modalShow: true,
            currentId: penugasanId,
        });
    }

    modalCloseHandler = () => {
        this.setState({
            modalShow: false,
            currentId: undefined,
        });
    };

    componentDidMount() {
        this.loadPenugasans();
    }

    loadPenugasans = async () => {
        const fetchedDaftarPenugasan = [];
        const response = await api.get("assignments/deadlines/");
        for (let key in response.data) {
            fetchedDaftarPenugasan.push({
                ...response.data[key],
            });
            console.log(response.data[key]);
        }
        fetchedDaftarPenugasan.sort((a, b) => {
            a = new Date(a.deadline.tenggat_waktu);
            b = new Date(b.deadline.tenggat_waktu);
            return a > b ? -1 : a < b ? 1 : 0;
        });
        this.setState({
            daftarPenugasan: fetchedDaftarPenugasan,
        });
    };

    finishPenugasan = async () => {
        const penugasanId = this.state.currentId;
        this.modalCloseHandler();
        const penugasanData = {
            status: 1,
        };
        try {
            const response = await api.put(
                "assignments/deadlines/" + penugasanId + "/",
                penugasanData
            );
            if (!response.status === 200) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({ successModalShow: true }, async () => {
                await this.loadPenugasans();
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    };

    successModalCloseHandler = () => {
        this.setState({
            successModalShow: false,
        });
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    changeRoute(idPenugasan) {
        this.props.history.push("/penugasan/ubah/" + idPenugasan);
    }

    render() {
        const renderPenugasans = this.state.daftarPenugasan.map((penugasan, idx) => (
            <Penugasan
                key={idx}
                idx={idx + 1}
                waktuRilis={new Date(penugasan.deadline.waktu_rilis).toLocaleString()}
                deadline={new Date(penugasan.deadline.tenggat_waktu).toLocaleString()}
                status={penugasan.status}
                fieldOfficer={penugasan.field_officer.nama}
                finish={() => this.modalShowHandler(penugasan.id)}
                edit={() => this.changeRoute(penugasan.id)}
            />
        ));
        return (
            <React.Fragment>
                <Content>
                    <Modal centered show={this.state.modalShow} onHide={this.modalCloseHandler}>
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Apakah Anda yakin ingin menyelesaikan penugasan ini?
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                <Button
                                    className="mx-2"
                                    onClick={this.finishPenugasan}
                                    variant="primary"
                                >
                                    Ya, Selesaikan
                                </Button>
                                <Button
                                    className="mx-2"
                                    onClick={this.modalCloseHandler}
                                    variant="light"
                                >
                                    Tidak
                                </Button>
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Penugasan berhasil diselesaikan!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Penugasan gagal diselesaikan!
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Daftar Penugasan</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Penugasan</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <h4 className="my-4">Semua Penugasan</h4>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Waktu Rilis</th>
                                            <th>Deadline</th>
                                            <th>Status</th>
                                            <th>Field Officer</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderPenugasans}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarPenugasan;
