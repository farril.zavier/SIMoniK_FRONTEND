import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import { api } from "../../../api";
import FieldOfficer from "./FieldOfficer";

import classes from "./DaftarFieldOfficer.module.css";

class DaftarFieldOfficer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fieldOfficers: [],
        };
    }

    componentDidMount() {
        this.loadFieldOfficers();
    }

    loadFieldOfficers = async () => {
        const fetchedFieldOfficers = [];
        const response = await api.get("assignments/field-officers/");
        for (let key in response.data) {
            fetchedFieldOfficers.push({
                ...response.data[key],
            });
        }
        this.setState({
            fieldOfficers: fetchedFieldOfficers,
        });
    };

    render() {
        const renderFieldOfficers = this.state.fieldOfficers.map((fieldOfficer, idx) => (
            <FieldOfficer
                key={idx}
                idx={idx + 1}
                nama={fieldOfficer.nama}
                no_hp={fieldOfficer.no_hp}
                username={fieldOfficer.username}
            />
        ));

        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Daftar Field Officer</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Semua Field Officer</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <br></br>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nomor Telepon</th>
                                            <th>Username</th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderFieldOfficers}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarFieldOfficer;
