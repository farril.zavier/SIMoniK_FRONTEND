import React from "react";

const FieldOfficer = (props) => {
    return (
        <tr>
            <td>{props.idx}</td>
            <td>{props.nama}</td>
            <td>{props.no_hp}</td>
            <td>{props.username}</td>
        </tr>
    );
};

export default FieldOfficer;
