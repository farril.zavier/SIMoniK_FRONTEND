import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Highcharts, { map } from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { api } from "../../../api";

import classes from "./ProdukPopuler.module.css";

class ProdukPopuler extends Component {
    constructor(props) {
        super(props);

        this.state = {
            periode: "",
            daerah: "",
            dateOptions: "",
            daftarProdukPopuler: [],
            produkPopulerChartOptions: {
                title: {
                    text: "Produk Populer",
                    style: {
                        fontSize: "24px",
                    },
                },
                chart: {
                    type: "column",
                },
                yAxis: {
                    title: {
                        text: "Unit",
                    },
                },
                xAxis: {
                    title: {
                        text: "Produk",
                    },
                    categories: [],
                },
                series: [{ name: "Unit Terjual", data: [] }],
            },
            rincianProduk: [],
        };
    }

    componentWillMount() {
        this.loadProdukPopuler();
        this.loadRincianProduk();
    }

    loadProdukPopuler = async () => {
        const fetchedProducts = [];
        const categories = [];
        const values = [];
        const response = await api.get("dashboard/produk-populer/");
        for (let key in response.data) {
            fetchedProducts.push({
                ...response.data[key],
            });
            categories.push(response.data[key].produk_depo__produk__nama);
            values.push(response.data[key].total_sold);
        }
        this.setState({
            daftarProdukPopuler: fetchedProducts,
            produkPopulerChartOptions: {
                xAxis: {
                    categories: categories,
                },
                series: [{ name: "Unit Terjual", data: values }],
            },
        });
    };

    loadRincianProduk = async () => {
        const fetchedProducts = [];
        const response = await api.get("dashboard/rincian-produk/");
        for (let key in response.data) {
            fetchedProducts.push({
                ...response.data[key],
            });
        }
        this.setState({
            rincianProduk: fetchedProducts,
        });
    };

    searchKeyChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            searchOption: "",
        });
    };

    searchOptionChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    radioButtonChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            periode: "",
            startMonth: "",
            endMonth: "",
        });
    };

    periodeChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    monthChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    render() {
        let renderRincianProduk;
        renderRincianProduk = this.state.rincianProduk.map((produk, idx) => {
            return (
                <tr>
                    <td className="align-middle">{idx + 1}</td>
                    <td className="align-middle">{produk.produk_depo__produk__nama}</td>
                    <td className="align-middle">{produk.produk_depo__produk__kategori__nama}</td>
                    <td className="align-middle">{produk.total_depos_selling}</td>
                    <td className="align-middle">{produk.total_units_sold + " Unit"}</td>
                    <td className="align-middle">
                        {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                        }).format(produk.avg_sell_price)}
                    </td>
                    <td>
                        <Button
                            variant="secondary"
                            href={"/produk-populer/detail/" + produk.produk_depo__produk_id + "/"}
                        >
                            Lihat Detail
                        </Button>
                    </td>
                </tr>
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Produk Populer</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Grafik Popularitas Produk</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <Form>
                                    <Form.Label>Cari Berdasarkan</Form.Label>
                                    <Form.Group as={Row} controlId="formFilterOption">
                                        <Col sm="4">
                                            <Form.Control
                                                as="select"
                                                onChange={this.periodeChangeHandler}
                                                name="periode"
                                                type="text"
                                                value={this.state.periode}
                                            >
                                                <option selected>-- Pilih Periode --</option>
                                                <option value="0">3 bulan</option>
                                                <option value="1">6 bulan</option>
                                                <option value="2">12 bulan</option>
                                            </Form.Control>
                                        </Col>
                                        <Col sm="4">
                                            <Form.Control
                                                as="select"
                                                onChange={this.daerahChangeHandler}
                                                name="daerah"
                                                type="text"
                                                value={this.state.daerah}
                                            >
                                                <option selected>-- Daerah --</option>
                                                <option value="0">Bojonegoro</option>
                                                <option value="1">Tuban</option>
                                            </Form.Control>
                                        </Col>
                                        {/* TODO integrate with backend */}
                                        <Form.Group>
                                            <Button variant="primary">Cari</Button>
                                        </Form.Group>
                                    </Form.Group>
                                </Form>
                                <Row className="mt-4 mb-5">
                                    <Col sm="2"></Col>
                                    <Col sm="8">
                                        <Card>
                                            <Card.Header>Grafik Popularitas Produk</Card.Header>
                                            <Card.Body>
                                                <Card.Text>
                                                    <HighchartsReact
                                                        highcharts={Highcharts}
                                                        options={
                                                            this.state.produkPopulerChartOptions
                                                        }
                                                    />
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col sm="2"></Col>
                                </Row>

                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Produk</th>
                                            <th>Kategori</th>
                                            <th>Jml. Depo Penjual</th>
                                            <th>Jml. Produk Terjual</th>
                                            <th>Rata-rata Harga Jual</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderRincianProduk}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default ProdukPopuler;
