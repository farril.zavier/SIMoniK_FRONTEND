import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { api } from "../../../api";

import classes from "./Dashboard.module.css";

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            daftarKeuntungan: [],
            daftarPenjualanProduk: [],
            daftarProdukPopuler: [],
            keuntunganBulanIni: undefined,
            penjualanCount: undefined,
            depoCount: undefined,
            foCount: undefined,
            keuntunganChartOptions: {
                title: {
                    text: "Grafik Keuntungan HARA Depo",
                    style: {
                        fontSize: "24px",
                    },
                },
                chart: {
                    type: "line",
                },
                yAxis: {
                    title: {
                        text: "Keuntungan",
                    },
                },
                xAxis: {
                    title: {
                        text: "Periode",
                    },
                    categories: [],
                },
                tooltip: {
                    pointFormat: "{series.name}: <b>Rp. {point.y}</b>",
                },
                series: [{ name: "Keuntungan", data: [] }],
            },
            penjualanProdukChartOptions: {
                title: {
                    text: "Penjualan Produk HARA Depo",
                    style: {
                        fontSize: "24px",
                    },
                },
                chart: {
                    type: "line",
                },
                yAxis: {
                    title: {
                        text: "Unit",
                    },
                },
                xAxis: {
                    title: {
                        text: "Periode",
                    },
                    categories: [],
                },
                tooltip: {
                    pointFormat: "{series.name}: <b>{point.y} Unit</b>",
                },
                series: [{ name: "Unit Terjual", data: [] }],
            },
            produkPopulerChartOptions: {
                title: {
                    text: "Produk Populer",
                    style: {
                        fontSize: "24px",
                    },
                },
                chart: {
                    type: "column",
                },
                yAxis: {
                    title: {
                        text: "Unit",
                    },
                },
                xAxis: {
                    title: {
                        text: "Produk",
                    },
                    categories: [],
                },
                series: [{ name: "Unit Terjual", data: [] }],
            },
        };
    }

    componentWillMount() {
        this.loadKeuntunganBulanIni();
        this.loadTotalPenjualan();
        this.loadTotalDepos();
        this.loadTotalFieldOfficers();
        this.loadKeuntungan();
        this.loadPenjualanProduk();
        this.loadProdukPopuler();
    }

    loadKeuntunganBulanIni = async () => {
        const response = await api.get("dashboard/keuntungan-bulan-ini/");
        if (response.data.total_projected_revenue === null) {
            this.setState({
                keuntunganBulanIni: "-",
            });
        } else {
            this.setState({
                keuntunganBulanIni: response.data.total_projected_revenue,
            });
        }
    };

    loadTotalPenjualan = async () => {
        const response = await api.get("dashboard/count-penjualan/");
        this.setState({
            penjualanCount: response.data.total_sold,
        });
    };

    loadTotalDepos = async () => {
        const response = await api.get("dashboard/count-depo/");
        this.setState({
            depoCount: response.data,
        });
    };

    loadTotalFieldOfficers = async () => {
        const response = await api.get("dashboard/count-fo/");
        this.setState({
            foCount: response.data,
        });
    };

    loadKeuntungan = async () => {
        const months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        const fetchedStockOpnames = [];
        const categories = [];
        const values = [];
        const response = await api.get("dashboard/keuntungan/");
        for (let key in response.data) {
            fetchedStockOpnames.push({
                ...response.data[key],
            });
            categories.push(months[response.data[key].month - 1] + " " + response.data[key].year);
            values.push(response.data[key].total_projected_revenue);
        }
        this.setState({
            daftarKeuntungan: fetchedStockOpnames,
            keuntunganChartOptions: {
                xAxis: {
                    title: {
                        text: "Periode",
                    },
                    categories: categories,
                },
                series: [{ name: "Keuntungan", data: values }],
            },
        });
    };

    loadPenjualanProduk = async () => {
        const months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        const fetchedStockOpnames = [];
        const categories = [];
        const values = [];
        const response = await api.get("dashboard/penjualan-produk/");
        for (let key in response.data) {
            fetchedStockOpnames.push({
                ...response.data[key],
            });
            categories.push(months[response.data[key].month - 1] + " " + response.data[key].year);
            values.push(response.data[key].total_sold);
        }
        this.setState({
            daftarPenjualanProduk: fetchedStockOpnames,
            penjualanProdukChartOptions: {
                xAxis: {
                    title: {
                        text: "Periode",
                    },
                    categories: categories,
                },
                series: [{ name: "Unit Terjual", data: values }],
            },
        });
    };

    loadProdukPopuler = async () => {
        const fetchedProducts = [];
        const categories = [];
        const values = [];
        const response = await api.get("dashboard/produk-populer/");
        for (let key in response.data) {
            fetchedProducts.push({
                ...response.data[key],
            });
            categories.push(response.data[key].produk_depo__produk__nama);
            values.push(response.data[key].total_sold);
        }
        this.setState({
            daftarProdukPopuler: fetchedProducts,
            produkPopulerChartOptions: {
                xAxis: {
                    categories: categories,
                },
                series: [{ name: "Unit Terjual", data: values }],
            },
        });
    };

    formatNumber(n) {
        var ranges = [
            { divider: 1e18, suffix: "E" },
            { divider: 1e15, suffix: "P" },
            { divider: 1e12, suffix: "T" },
            { divider: 1e9, suffix: "G" },
            { divider: 1e6, suffix: "M" },
            { divider: 1e3, suffix: "K" },
        ];
        for (var i = 0; i < ranges.length; i++) {
            if (n >= ranges[i].divider) {
                return (n / ranges[i].divider).toFixed(1) + ranges[i].suffix;
            }
        }
        return n.toString();
    }

    render() {
        const renderProdukPopuler = this.state.daftarProdukPopuler.map((produk, idx) => {
            return (
                <tr>
                    <td className="align-middle">{idx + 1}</td>
                    <td className="align-middle">{produk.produk_depo__produk__nama}</td>
                    <td className="align-middle">{produk.produk_depo__produk__kategori__nama}</td>
                    <td className="align-middle">
                        {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                        }).format(produk.avg_sell_price)}
                    </td>
                </tr>
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Dashboard</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Dashboard</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <Container fluid className="mt-4 mb-5">
                                    <Row>
                                        <Col sm="3">
                                            <Card bg="success" text="white">
                                                <Card.Header className="h5">
                                                    KEUNTUNGAN BULAN INI
                                                </Card.Header>
                                                <Card.Body>
                                                    <Card.Text className="display-3 mt-n3 mb-n2">
                                                        {this.state.keuntunganBulanIni !==
                                                            undefined &&
                                                        this.state.keuntunganBulanIni !== "-"
                                                            ? this.formatNumber(
                                                                  this.state.keuntunganBulanIni
                                                              )
                                                            : 0}
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                        <Col sm="3">
                                            <Card bg="warning" text="white">
                                                <Card.Header className="h5">
                                                    TOTAL PENJUALAN
                                                </Card.Header>
                                                <Card.Body>
                                                    <Card.Text className="display-3 mt-n3 mb-n2">
                                                        {this.state.penjualanCount !== undefined ? (
                                                            this.formatNumber(
                                                                this.state.penjualanCount
                                                            )
                                                        ) : (
                                                            <React.Fragment></React.Fragment>
                                                        )}
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                        <Col sm="3">
                                            <Card bg="info" text="white">
                                                <Card.Header className="h5">TOTAL DEPO</Card.Header>
                                                <Card.Body>
                                                    <Card.Text className="display-3 mt-n3 mb-n2">
                                                        {this.state.depoCount}
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                        <Col sm="3">
                                            <Card bg="danger" text="white">
                                                <Card.Header className="h5">
                                                    TOTAL FIELD OFFICER
                                                </Card.Header>
                                                <Card.Body>
                                                    <Card.Text className="display-3 mt-n3 mb-n2">
                                                        {this.state.foCount}
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    </Row>
                                </Container>

                                <Row>
                                    <Col sm="6">
                                        <Card>
                                            <Card.Header>Ringkasan Keuntungan</Card.Header>
                                            <Card.Body>
                                                <Card.Text>
                                                    <HighchartsReact
                                                        highcharts={Highcharts}
                                                        options={this.state.keuntunganChartOptions}
                                                    />
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col sm="6">
                                        <Card>
                                            <Card.Header>Ringkasan Penjualan Produk</Card.Header>
                                            <Card.Body>
                                                <Card.Text>
                                                    <HighchartsReact
                                                        highcharts={Highcharts}
                                                        options={
                                                            this.state.penjualanProdukChartOptions
                                                        }
                                                    />
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>

                                <Row className="mt-5 mb-3">
                                    <Col sm="12">
                                        <Card>
                                            <Card.Header>
                                                Top 5 Produk Populer (Semua Daerah)
                                            </Card.Header>
                                            <Card.Body>
                                                <Card.Text>
                                                    <Row>
                                                        <Col sm="6">
                                                            <Table bordered hover className="h-100">
                                                                <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Nama Produk</th>
                                                                        <th>Kategori</th>
                                                                        <th>
                                                                            Rata-rata harga jual
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>{renderProdukPopuler}</tbody>
                                                            </Table>
                                                        </Col>
                                                        <Col sm="6">
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={
                                                                    this.state
                                                                        .produkPopulerChartOptions
                                                                }
                                                            />
                                                        </Col>
                                                    </Row>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default Dashboard;
