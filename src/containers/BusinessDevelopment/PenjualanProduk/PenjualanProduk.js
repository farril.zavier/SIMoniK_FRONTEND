import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { api } from "../../../api";

import classes from "./PenjualanProduk.module.css";

class PenjualanProduk extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchKey: "",
            searchOption: "",
            dateOptions: "",
            periode: "",
            startMonth: "",
            endMonth: "",
            daftarPenjualanProduk: [],
            penjualanProdukChartOptions: {
                title: {
                    text: "Penjualan Produk HARA Depo",
                    style: {
                        fontSize: "24px",
                    },
                },
                chart: {
                    type: "line",
                },
                yAxis: {
                    title: {
                        text: "Unit",
                    },
                },
                xAxis: {
                    title: {
                        text: "Periode",
                    },
                    categories: [],
                },
                tooltip: {
                    pointFormat: "{series.name}: <b>{point.y} Unit</b>",
                },
                series: [{ name: "Unit Terjual", data: [] }],
            },
            rincianPenjualan: [],
        };
    }

    componentWillMount() {
        this.loadPenjualanProduk();
        this.loadRincianPenjualan();
    }

    loadPenjualanProduk = async () => {
        const months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        const fetchedStockOpnames = [];
        const categories = [];
        const values = [];
        const response = await api.get("dashboard/penjualan-produk/");
        for (let key in response.data) {
            fetchedStockOpnames.push({
                ...response.data[key],
            });
            categories.push(months[response.data[key].month - 1] + " " + response.data[key].year);
            values.push(response.data[key].total_sold);
        }
        this.setState({
            daftarPenjualanProduk: fetchedStockOpnames,
            penjualanProdukChartOptions: {
                xAxis: {
                    title: {
                        text: "Periode",
                    },
                    categories: categories,
                },
                series: [{ name: "Unit Terjual", data: values }],
            },
        });
    };

    loadRincianPenjualan = async () => {
        const fetchedProducts = [];
        const response = await api.get("dashboard/rincian-penjualan/");
        for (let key in response.data) {
            fetchedProducts.push({
                ...response.data[key],
            });
        }
        this.setState({
            rincianPenjualan: fetchedProducts,
        });
    };

    searchKeyChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            searchOption: "",
        });
    };

    searchOptionChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    radioButtonChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            periode: "",
            startMonth: "",
            endMonth: "",
        });
    };

    periodeChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    monthChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    render() {
        let renderSearchOptions;
        if (this.state.searchKey === "0") {
            renderSearchOptions = (
                <React.Fragment>
                    <option value="-1" selected>
                        -- Pilih Sub-Penyaring --
                    </option>
                    <option value="0">Fungisida</option>
                    <option value="1">Pestisida</option>
                    <option value="2">Herbisida</option>
                </React.Fragment>
            );
        } else if (this.state.searchKey === "1") {
            renderSearchOptions = (
                <React.Fragment>
                    <option value="-1" selected>
                        -- Pilih Sub-Penyaring --
                    </option>
                    <option value="0">Bojonegoro</option>
                    <option value="1">Tuban</option>
                </React.Fragment>
            );
        } else {
            renderSearchOptions = (
                <option value="-1" selected>
                    -- Pilih Sub-Penyaring --
                </option>
            );
        }

        let renderPeriodeOptions;
        if (this.state.dateOptions === "periode") {
            renderPeriodeOptions = (
                <React.Fragment>
                    <option value="-1">Pilih periode ...</option>
                    <option value="0">3 bulan</option>
                    <option value="1">6 bulan</option>
                    <option value="2">12 bulan</option>
                </React.Fragment>
            );
        } else {
            renderPeriodeOptions = (
                <option value="-1" selected>
                    Pilih periode ...
                </option>
            );
        }

        let renderJangkaWaktu;
        if (this.state.dateOptions === "jangkaWaktu") {
            renderJangkaWaktu = (
                <React.Fragment>
                    <Col sm="5">
                        <Form.Control
                            onChange={this.monthChangeHandler}
                            name="startMonth"
                            type="month"
                            value={this.state.startMonth}
                        />
                    </Col>
                    <Col sm="1" className="text-center">
                        <div className="d-flex h-100 align-items-center justify-content-center">
                            ----
                        </div>
                    </Col>
                    <Col sm="5">
                        <Form.Control
                            onChange={this.monthChangeHandler}
                            name="endMonth"
                            type="month"
                            value={this.state.endMonth}
                        />
                    </Col>
                </React.Fragment>
            );
        } else {
            renderJangkaWaktu = (
                <React.Fragment>
                    <Col sm="5">
                        <Form.Control
                            disabled
                            onChange={this.monthChangeHandler}
                            name="startMonth"
                            type="month"
                            value={this.state.startMonth}
                        />
                    </Col>
                    <Col sm="1" className="text-center">
                        <div className="d-flex h-100 align-items-center justify-content-center">
                            ----
                        </div>
                    </Col>
                    <Col sm="5">
                        <Form.Control
                            disabled
                            onChange={this.monthChangeHandler}
                            name="endMonth"
                            type="month"
                            value={this.state.endMonth}
                        />
                    </Col>
                </React.Fragment>
            );
        }

        let renderRincianPenjualan;
        renderRincianPenjualan = this.state.rincianPenjualan.map((produk, idx) => {
            return (
                <tr>
                    <td className="align-middle">{idx + 1}</td>
                    <td className="align-middle">{produk.produk_depo__produk__nama}</td>
                    <td className="align-middle">{produk.total_sold + " Unit"}</td>
                    <td>
                        <Button
                            variant="secondary"
                            href={"/penjualan-produk/detail/" + produk.produk_depo__produk_id + "/"}
                        >
                            Lihat Detail
                        </Button>
                    </td>
                </tr>
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Penjualan Produk</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Grafik Penjualan</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <Form>
                                    <Form.Label>Cari Berdasarkan</Form.Label>
                                    <Form.Group as={Row} controlId="formFilterOption">
                                        <Col sm="4">
                                            <Form.Control
                                                as="select"
                                                onChange={this.searchKeyChangeHandler}
                                                name="searchKey"
                                                type="text"
                                                value={this.state.searchKey}
                                            >
                                                <option selected>-- Pilih Penyaring --</option>
                                                <option value="0">Kategori</option>
                                                <option value="1">Daerah</option>
                                            </Form.Control>
                                        </Col>
                                        <Col sm="4">
                                            <Form.Control
                                                as="select"
                                                onChange={this.searchOptionChangeHandler}
                                                name="searchOption"
                                                type="text"
                                                value={this.state.searchOption}
                                            >
                                                {renderSearchOptions}
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="formFilterDate">
                                        <Col sm="4">
                                            <Form.Check
                                                inline
                                                type="radio"
                                                aria-label="Periode"
                                                name="dateOptions"
                                                value="periode"
                                                className="mr-1"
                                                onChange={this.radioButtonChangeHandler}
                                            />
                                            Periode
                                            <Form.Group
                                                as={Row}
                                                controlId="formPeriode"
                                                className="mt-2"
                                            >
                                                <Col sm="1"></Col>
                                                <Col sm="11">
                                                    <Form.Control
                                                        as="select"
                                                        onChange={this.periodeChangeHandler}
                                                        name="periode"
                                                        type="text"
                                                        value={this.state.periode}
                                                    >
                                                        {renderPeriodeOptions}
                                                    </Form.Control>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row} controlId="formSubmitButton">
                                                <Col sm="1"></Col>
                                                <Col sm="11">
                                                    {/* TODO integrate with backend */}
                                                    <Button variant="primary">Cari</Button>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                        <Col sm="6">
                                            <Form.Check
                                                inline
                                                type="radio"
                                                aria-label="Jangka Waktu"
                                                name="dateOptions"
                                                value="jangkaWaktu"
                                                className="mr-1"
                                                onChange={this.radioButtonChangeHandler}
                                            />
                                            Jangka Waktu
                                            <Form.Group
                                                as={Row}
                                                controlId="formJangkaWaktuGroup"
                                                className="mt-2"
                                            >
                                                <Col sm="1"></Col>
                                                <Col sm="11">
                                                    <Form.Group
                                                        as={Row}
                                                        controlId="formJangkaWaktu"
                                                    >
                                                        {renderJangkaWaktu}
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Col>
                                    </Form.Group>
                                </Form>
                                <Row className="mt-3 mb-5">
                                    <Col sm="2"></Col>
                                    <Col sm="8">
                                        <Card>
                                            <Card.Header>Grafik Penjualan</Card.Header>
                                            <Card.Body>
                                                <Card.Text>
                                                    <HighchartsReact
                                                        highcharts={Highcharts}
                                                        options={
                                                            this.state.penjualanProdukChartOptions
                                                        }
                                                    />
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col sm="2"></Col>
                                </Row>

                                <Card.Title>Rincian Penjualan</Card.Title>
                                <div className={classes.divider}></div>
                                <Row className="mt-4 mb-2">
                                    <Col sm="2"></Col>
                                    <Col sm="8">
                                        <Table bordered hover>
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Produk</th>
                                                    <th>Penjualan</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>{renderRincianPenjualan}</tbody>
                                        </Table>
                                    </Col>
                                    <Col sm="2"></Col>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default PenjualanProduk;
