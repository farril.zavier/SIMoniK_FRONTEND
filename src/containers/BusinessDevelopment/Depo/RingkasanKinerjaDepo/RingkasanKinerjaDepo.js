import React, { Component } from "react";
import Content from "../../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { api } from "../../../../api";

import classes from "./RingkasanKinerjaDepo.module.css";

class RingkasanKinerjaDepo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            idDepo: props.match.params.id,
            depo: {},
            daftarKategori: [],
            productTypePercentageChartOptions: {
                title: {
                    text: "% Tipe Produk",
                    style: {
                        fontSize: "24px",
                    },
                },
                chart: {
                    type: "pie",
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: "pointer",
                        dataLabels: {
                            enabled: false,
                        },
                        showInLegend: true,
                    },
                },
                tooltip: {
                    pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
                },
                series: [
                    {
                        name: "Kategori Produk",
                        data: [],
                    },
                ],
            },
            daftarRingkasanProdukDepo: [],
            ringkasanProdukDepo: [],
            revenueStore: undefined,
            marginStore: undefined,
        };
    }

    componentWillMount() {
        this.loadDetailDepo();
        this.loadRingkasanProdukDepo();
    }

    loadDetailDepo = async () => {
        let idDepo = this.state.idDepo;
        const response = await api.get("dashboard/detail-depo/" + idDepo + "/");
        const depo = response.data;
        const daftarKategori = depo.daftar_kategori;
        const temp = [...new Set(daftarKategori.map((kategori) => kategori.nama))];
        const distinctCategories = [];
        for (var kategori of temp) {
            distinctCategories.push({
                nama: kategori,
                jumlah: 0,
            });
        }
        console.log(distinctCategories);
        this.setState({
            depo: depo,
            daftarKategori: distinctCategories,
        });
    };

    loadRingkasanProdukDepo = async () => {
        const fetchedRingkasanProdukDepo = [];
        const response = await api.get("dashboard/ringkasan-produk-depo/");
        for (let key in response.data) {
            fetchedRingkasanProdukDepo.push({
                ...response.data[key],
            });
        }
        const idDepo = Number(this.state.idDepo);
        let startIndexFound = false;
        let startIndex,
            endIndex = -1;
        for (let i = 0; i < fetchedRingkasanProdukDepo.length; i++) {
            if (!startIndexFound && fetchedRingkasanProdukDepo[i].produk_depo__depo_id === idDepo) {
                startIndexFound = true;
                startIndex = i;
            }
            if (startIndexFound && fetchedRingkasanProdukDepo[i].produk_depo__depo_id !== idDepo) {
                endIndex = i;
                break;
            }
            if (
                startIndexFound &&
                i === fetchedRingkasanProdukDepo.length - 1 &&
                fetchedRingkasanProdukDepo[i].produk_depo__depo_id === idDepo
            ) {
                endIndex = i + 1;
                break;
            }
        }
        let ringkasanProdukDepo;
        if (startIndex !== -1 && endIndex !== -1) {
            ringkasanProdukDepo = fetchedRingkasanProdukDepo.slice(startIndex, endIndex);
            const values = [
                {
                    name: "Moluskisida",
                    y: 7.1,
                },
                {
                    name: "Fungisida",
                    y: 14.3,
                },
                {
                    name: "Herbisida",
                    y: 19,
                },
                {
                    name: "Insektisida",
                    y: 59.5,
                },
            ];
            this.setState({
                daftarRingkasanProdukDepo: fetchedRingkasanProdukDepo,
                ringkasanProdukDepo: ringkasanProdukDepo,
                revenueStore: ringkasanProdukDepo[0].revenue_store,
                marginStore: ringkasanProdukDepo[0].margin_store,
                productTypePercentageChartOptions: {
                    series: [
                        {
                            name: "Kategori Produk",
                            data: values,
                        },
                    ],
                },
            });
        } else {
            this.setState({
                daftarRingkasanProdukDepo: fetchedRingkasanProdukDepo,
                ringkasanProdukDepo: [],
                revenueStore: null,
                marginStore: null,
            });
        }
    };

    render() {
        let renderProducts;
        renderProducts = this.state.ringkasanProdukDepo.map((ringkasan, idx) => {
            return (
                <tr>
                    <td className="align-middle">{idx + 1}</td>
                    <td className="align-middle">{ringkasan.produk_depo__produk__nama}</td>
                </tr>
            );
        });
        let renderCategories;
        renderCategories = this.state.daftarKategori.map((kategori, idx) => {
            if (idx === this.state.daftarKategori.length - 1) {
                return kategori.nama;
            } else {
                return kategori.nama + "; ";
            }
        });
        let renderProductData;
        renderProductData = this.state.ringkasanProdukDepo.map((ringkasan, idx) => {
            return (
                <tr>
                    <td>{ringkasan.produk_depo__produk__nama}</td>
                    <td>{ringkasan.produk_depo__produk__kategori__nama}</td>
                    <td>
                        {ringkasan.quantity_so1 !== null ? (
                            ringkasan.quantity_so1 + " Unit"
                        ) : (
                            <React.Fragment>0 Unit</React.Fragment>
                        )}
                    </td>
                    <td>
                        {ringkasan.quantity_so2 !== null ? (
                            ringkasan.quantity_so2 + " Unit"
                        ) : (
                            <React.Fragment>0 Unit</React.Fragment>
                        )}
                    </td>
                    <td>
                        {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                        }).format(ringkasan.produk_depo__harga_beli)}
                    </td>
                    <td>
                        {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                        }).format(ringkasan.produk_depo__harga_jual)}
                    </td>
                    <td>
                        {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                        }).format(ringkasan.total_projected_revenue)}
                    </td>
                </tr>
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/daftar-depo">Depo</Breadcrumb.Item>
                        <Breadcrumb.Item active>Lihat Detail</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Ringkasan Kinerja Depo</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <div className="my-4 d-flex justify-content-between">
                                    <h4>{this.state.depo.nama}</h4>
                                    <Button
                                        href="/daftar-depo/ringkasan-kinerja/perbandingan-produk"
                                        variant="primary"
                                    >
                                        Perbandingan Produk
                                    </Button>
                                </div>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Nama Kiosk
                                    </Col>
                                    <Col sm="4">: {this.state.depo.nama}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Claim Revenue Store
                                    </Col>
                                    <Col sm="4">
                                        :{" "}
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(this.state.depo.omset)}
                                    </Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Pemilik Kiosk
                                    </Col>
                                    <Col sm="4">: {this.state.depo.nama_pemilik}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Revenue Store
                                    </Col>
                                    <Col sm="4">
                                        :{" "}
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(this.state.revenueStore)}
                                    </Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Kategori Kiosk
                                    </Col>
                                    <Col sm="4">: {renderCategories}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Margin Store
                                    </Col>
                                    <Col sm="4">
                                        :{" "}
                                        {Number(this.state.marginStore).toLocaleString("en-US", {
                                            style: "percent",
                                            minimumFractionDigits: 2,
                                        })}
                                    </Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Kecamatan
                                    </Col>
                                    <Col sm="4">: {this.state.depo.kecamatan}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Desa
                                    </Col>
                                    <Col sm="4">: {this.state.depo.desa}</Col>
                                </Row>

                                <Row className="mt-4 align-items-center">
                                    <Col sm="6">
                                        <Table bordered hover className="h-100">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Produk</th>
                                                </tr>
                                            </thead>
                                            <tbody>{renderProducts}</tbody>
                                        </Table>
                                    </Col>
                                    <Col sm="6">
                                        <HighchartsReact
                                            highcharts={Highcharts}
                                            options={this.state.productTypePercentageChartOptions}
                                        />
                                    </Col>
                                </Row>
                                <Row className="mt-3">
                                    <Col>
                                        <Table bordered hover responsive>
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Kategori</th>
                                                    <th>Quantity SO #1</th>
                                                    <th>Quantity SO #2</th>
                                                    <th>Purchase Price</th>
                                                    <th>Sell Price</th>
                                                    <th>Revenue</th>
                                                </tr>
                                            </thead>
                                            <tbody>{renderProductData}</tbody>
                                        </Table>
                                    </Col>
                                </Row>

                                <Row className="justify-content-center">
                                    <Button
                                        href="/daftar-depo"
                                        variant="primary"
                                        className="mt-4 mb-3"
                                    >
                                        Kembali
                                    </Button>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default RingkasanKinerjaDepo;
