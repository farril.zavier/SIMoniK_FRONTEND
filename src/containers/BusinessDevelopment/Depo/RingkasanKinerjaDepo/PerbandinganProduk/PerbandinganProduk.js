import React, { Component } from "react";
import Content from "../../../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

import classes from "./PerbandinganProduk.module.css";

const products = [
    "Balistic 50 SC",
    "VALARTA 600 SL",
    "RAGE 90 SP",
    "TRIMMER 18 EC",
    "QUICKPRO 276 SL",
];

const options = {
    title: {
        text: "Perbandingan Produk antar Stock Opname",
        style: {
            fontSize: "24px",
        },
    },
    chart: {
        type: "column",
    },
    yAxis: {
        title: {
            text: "Quantity",
        },
    },
    xAxis: {
        title: {
            text: "Produk",
        },
        categories: products,
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
            '<tr><td style="color:{series.color};padding:0 4px 0 0">{series.name}:</td>' +
            '<td style="padding:0"><b>{point.y} Unit</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
    },
    series: [
        { name: "Quantity SO1", data: [65, 8, 90, 81, 40] },
        { name: "Quantity SO2", data: [21, 48, 40, 19, 100] },
    ],
};

class PerbandinganProduk extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [
                {
                    nama: "Balistic 50 SC",
                },
                {
                    nama: "VALARTA 600 SL",
                },
                {
                    nama: "RAGE 90 SP",
                },
                {
                    nama: "TRIMMER 18 EC",
                },
                {
                    nama: "QUICKPRO 276 SL",
                },
            ],
        };
    }

    render() {
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/daftar-depo">Depo</Breadcrumb.Item>
                        <Breadcrumb.Item href="/daftar-depo/ringkasan-kinerja">
                            Lihat Detail
                        </Breadcrumb.Item>
                        <Breadcrumb.Item active>Perbandingan Produk</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Ringkasan Kinerja Depo</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <h4 className="my-4">Depo Amanah Tani</h4>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Nama Kiosk
                                    </Col>
                                    <Col sm="4">: Amanah Tani</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Claim Revenue Store
                                    </Col>
                                    <Col sm="4">: Rp 2,000,000</Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Pemilik Kiosk
                                    </Col>
                                    <Col sm="4">: Nur Shidiq</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Revenue Store
                                    </Col>
                                    <Col sm="4">: Rp 51,154,600</Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Kategori Kiosk
                                    </Col>
                                    <Col sm="4">: Obat Pertanian; Bibit Pertanian</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Margin Store
                                    </Col>
                                    <Col sm="4">: 6.36%</Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Kecamatan
                                    </Col>
                                    <Col sm="4">: Baureno</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Desa
                                    </Col>
                                    <Col sm="4">: Pomahan</Col>
                                </Row>

                                <Row className="mt-4">
                                    <Col sm="1"></Col>
                                    <Col sm="10">
                                        <HighchartsReact
                                            highcharts={Highcharts}
                                            options={options}
                                        />
                                    </Col>
                                    <Col sm="1"></Col>
                                </Row>

                                <Row className="justify-content-center">
                                    <Button
                                        href="/daftar-depo/ringkasan-kinerja"
                                        variant="primary"
                                        className="mt-4 mb-3"
                                    >
                                        Kembali
                                    </Button>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default PerbandinganProduk;
