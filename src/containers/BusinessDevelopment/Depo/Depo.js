import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { api } from "../../../api";

import classes from "./Depo.module.css";

class Depo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            periode: "",
            daerah: "",
            dateOptions: "",
            depos: [],
        };
    }

    componentWillMount() {
        this.loadDepos();
    }

    loadDepos = async () => {
        const fetchedDepos = [];
        const response = await api.get("dashboard/daftar-depo/");
        for (let key in response.data) {
            fetchedDepos.push({
                ...response.data[key],
            });
        }
        this.setState({
            depos: fetchedDepos,
        });
    };

    searchKeyChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            searchOption: "",
        });
    };

    searchOptionChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    radioButtonChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            periode: "",
            startMonth: "",
            endMonth: "",
        });
    };

    periodeChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    monthChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    render() {
        let renderDaftarDepo;
        renderDaftarDepo = this.state.depos.map((depo, idx) => {
            return (
                <tr>
                    <td className="align-middle">{idx + 1}</td>
                    <td className="align-middle">{depo.nama}</td>
                    <td className="align-middle">{depo.kode_toko}</td>
                    <td className="align-middle">{depo.kabupaten}</td>
                    <td>
                        <Button
                            variant="secondary"
                            href={"/daftar-depo/ringkasan-kinerja/" + depo.id + "/"}
                        >
                            Lihat Detail
                        </Button>
                    </td>
                </tr>
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Depo</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Depo</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <Form>
                                    <Form.Label>Cari Berdasarkan</Form.Label>
                                    <Form.Group as={Row} controlId="formFilterOption">
                                        <Col sm="4">
                                            <Form.Control
                                                as="select"
                                                onChange={this.periodeChangeHandler}
                                                name="periode"
                                                type="text"
                                                value={this.state.periode}
                                            >
                                                <option selected>-- Pilih Periode --</option>
                                                <option value="0">3 bulan</option>
                                                <option value="1">6 bulan</option>
                                                <option value="2">12 bulan</option>
                                            </Form.Control>
                                        </Col>
                                        <Col sm="4">
                                            <Form.Control
                                                as="select"
                                                onChange={this.daerahChangeHandler}
                                                name="daerah"
                                                type="text"
                                                value={this.state.daerah}
                                            >
                                                <option selected>-- Daerah --</option>
                                                <option value="0">Bojonegoro</option>
                                                <option value="1">Tuban</option>
                                            </Form.Control>
                                        </Col>
                                        {/* TODO integrate with backend */}
                                        <Form.Group>
                                            <Button variant="primary">Cari</Button>
                                        </Form.Group>
                                    </Form.Group>
                                </Form>

                                <Card.Title className="mt-5">Semua Depo</Card.Title>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Depo</th>
                                            <th>Kode Depo</th>
                                            <th>Kabupaten</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderDaftarDepo}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default Depo;
