import React, { Component } from "react";
import Content from "../../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { api } from "../../../../api";

import classes from "./DetailProduk.module.css";

class DetailProduk extends Component {
    constructor(props) {
        super(props);

        this.state = {
            idProduk: props.match.params.id,
            daftarDetailProduk: [],
            detailProduk: {},
        };
    }

    componentDidMount() {
        this.loadDetailProduk();
    }

    loadDetailProduk = async () => {
        const fetchedProductDetails = [];
        const response = await api.get("dashboard/detail-produk/");
        for (let key in response.data) {
            fetchedProductDetails.push({
                ...response.data[key],
            });
        }
        const idProduk = Number(this.state.idProduk);
        const matchingProduct = fetchedProductDetails.find((product) => {
            return product.produk_depo__produk_id === idProduk;
        });
        this.setState({
            daftarDetailProduk: fetchedProductDetails,
            detailProduk: matchingProduct,
        });
    };

    render() {
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/keuntungan">Keuntungan</Breadcrumb.Item>
                        <Breadcrumb.Item active>
                            {this.state.detailProduk.produk_depo__produk__nama}
                        </Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Detail Produk</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <h4 className="my-4">
                                    {this.state.detailProduk.produk_depo__produk__nama}
                                </h4>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        ID Produk
                                    </Col>
                                    <Col sm="4">
                                        : {this.state.detailProduk.produk_depo__produk_id}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Tanggal Akhir Ijin
                                    </Col>
                                    <Col sm="4">
                                        :{" "}
                                        {
                                            this.state.detailProduk
                                                .produk_depo__produk__tgl_akhir_izin
                                        }
                                    </Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Kategori
                                    </Col>
                                    <Col sm="4">
                                        :{" "}
                                        {
                                            this.state.detailProduk
                                                .produk_depo__produk__kategori__nama
                                        }
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Total Depo Menjual Produk
                                    </Col>
                                    <Col sm="4">
                                        : {this.state.detailProduk.total_depos_selling}
                                    </Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Perusahaan
                                    </Col>
                                    <Col sm="4">
                                        : {this.state.detailProduk.produk_depo__produk__perusahaan}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Total Produk Terjual
                                    </Col>
                                    <Col sm="4">: {this.state.detailProduk.total_units_sold}</Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Deskripsi Singkat
                                    </Col>
                                    <Col sm="4">
                                        : {this.state.detailProduk.produk_depo__produk__deskripsi}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Total Keuntungan
                                    </Col>
                                    <Col sm="4">
                                        :{" "}
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(this.state.detailProduk.total_projected_revenue)}
                                    </Col>
                                </Row>
                                <Row className="justify-content-center">
                                    <Button
                                        href="/keuntungan"
                                        variant="primary"
                                        className="mt-4 mb-3"
                                    >
                                        Kembali
                                    </Button>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DetailProduk;
