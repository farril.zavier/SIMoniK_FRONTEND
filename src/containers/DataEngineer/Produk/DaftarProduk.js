import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";

import classes from "./DaftarProduk.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { api } from "../../../api";
import ProdukTable from "./ProdukTable";

class DaftarProduk extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            currentId: undefined,
            categorys: [],
            filter: 0,
            category: 0,
        };
    }

    componentDidMount() {
        this.loadProducts();
        this.loadCategorys();
    }

    loadProducts = async () => {
        const fetchedProducts = [];
        const response = await api.get("produk/");
        for (let key in response.data) {
            fetchedProducts.push({
                ...response.data[key],
            });
        }
        this.setState({
            products: fetchedProducts,
        });
    };

    loadCategorys = async () => {
        const fetchedCategorys = [];
        const response = await api.get("kategori/");
        for (let key in response.data) {
            fetchedCategorys.push({
                ...response.data[key],
            });
        }
        this.setState({
            categorys: fetchedCategorys,
        });
    };

    filterHandler = (event) => {
        const { name, value } = event.target;
        console.log(name + value);
        this.setState({
            [name]: value,
        });
    };

    submitHandler = (event) => {
        event.preventDefault();
        this.setState({ filter: this.state.category });
    };

    render() {
        const renderProducts = this.state.products
            .filter(
                (product, idx) => this.state.filter == 0 || product.kategori == this.state.filter
            )
            .map((product, idx) => (
                <ProdukTable
                    key={idx}
                    idx={idx + 1}
                    nama={product.nama}
                    perusahaan={product.perusahaan}
                    kategori={product.kategori_nama}
                    id={product.id}
                />
            ));

        const renderCategorys = this.state.categorys.map((category, idx) => {
            return (
                <option key={idx} value={category.id}>
                    {category.nama}
                </option>
            );
        });

        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Produk</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Produk</Card.Title>
                            <div className={classes.divider}></div>

                            <Button href="/produk/tambah" variant="outline-primary">
                                + Tambah Produk
                            </Button>
                            <div className={classes.divider}></div>

                            <Card.Text>
                                <p> Cari Berdasarkan: </p>
                                <Form inline onSubmit={this.submitHandler}>
                                    <Form.Control
                                        onChange={this.filterHandler}
                                        name="category"
                                        as="select"
                                    >
                                        <option selected value={0}>
                                            -- Semua Kategori --
                                        </option>
                                        {renderCategorys}
                                    </Form.Control>
                                    <Button className="ml-3" variant="primary" type="submit">
                                        Cari
                                    </Button>
                                </Form>

                                <br></br>

                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Produk</th>
                                            <th>Perusahaan</th>
                                            <th>Kategori</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderProducts}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarProduk;
