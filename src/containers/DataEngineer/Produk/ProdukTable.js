import React from "react";
import Button from "react-bootstrap/Button";

import classes from "./ProdukTable.module.css";

const ProdukTable = (props) => {
    return (
        <tr>
            <td>{props.idx}</td>
            {/* <td>{props.sku}</td> */}
            <td>{props.nama}</td>
            {/* <td>{props.ukuran}</td> */}
            <td>{props.perusahaan}</td>
            <td>{props.kategori}</td>
            {/* <td>{props.tgl_akhir_izin}</td> */}
            {/* <td>{props.deskripsi}</td> */}
            <td>
                <Button href={"produk/detail/" + props.id} variant="primary">
                    Lihat Detail
                </Button>
            </td>
            <td>
                <Button href={"produk/ubah/" + props.id} variant="secondary">
                    Ubah Detail Produk
                </Button>
            </td>
        </tr>
    );
};

export default ProdukTable;
