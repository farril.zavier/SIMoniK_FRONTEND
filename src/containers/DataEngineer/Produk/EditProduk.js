import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

import classes from "./EditProduk.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { api } from "../../../api";

class EditProduk extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            successModalShow: false,
            failModalShow: false,
            idProduk: props.match.params.id,
            product: {},
            categorys: [],
            nama: "",
            sku: "",
            ukuran: "",
            perusahaan: "",
            kategori: "0",
            tgl_akhir_izin: "",
            deskripsi: "",
        };
    }

    componentDidMount() {
        this.loadCategorys();
    }

    loadCategorys = async () => {
        const fetchedCategorys = [];
        const response = await api.get("kategori/");
        for (let key in response.data) {
            fetchedCategorys.push({
                ...response.data[key],
            });
        }
        this.setState(
            {
                categorys: fetchedCategorys,
            },
            () => {
                this.loadProduct();
            }
        );
    };

    loadProduct = async () => {
        let idProduk = this.state.idProduk;
        const response = await api.get("produk/" + idProduk + "/");
        const product = response.data;
        this.setState({
            product: product,
            nama: product.nama,
            sku: product.sku,
            ukuran: product.ukuran,
            perusahaan: product.perusahaan,
            kategori: product.kategori.id,
            tgl_akhir_izin: product.tgl_akhir_izin,
            deskripsi: product.deskripsi,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.reload();
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.editProduk();
    };

    async editProduk() {
        const product = {
            nama: this.state.nama,
            sku: this.state.sku,
            ukuran: this.state.ukuran,
            perusahaan: this.state.perusahaan,
            kategori: this.state.kategori,
            tgl_akhir_izin: this.state.tgl_akhir_izin,
            deskripsi: this.state.deskripsi,
        };
        try {
            let idProduk = this.state.idProduk;
            const response = await api.put("produk/" + idProduk + "/", product);
            if (!response.status === 200) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    changeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    getObject(val, arr, prop) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][prop] === val) {
                return arr[i];
            }
        }
        return -1;
    }

    render() {
        const renderCategorys = this.state.categorys.map((kategori, idx) => {
            return (
                <option key={idx} value={kategori.id}>
                    {kategori.nama}
                </option>
            );
        });

        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Produk berhasil diubah!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Produk gagal diubah!
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/produk">Produk</Breadcrumb.Item>
                        <Breadcrumb.Item active>{this.state.product.nama}</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Text>
                                <h4>Produk</h4>
                                <div className={classes.divider}></div>
                                <h5>
                                    <center>Data Produk</center>
                                </h5>
                                <div className={classes.divider}></div>
                                <Form onSubmit={this.handleSubmit}>
                                    <Row>
                                        <Col md={{ span: 6 }}>
                                            <h5>
                                                <strong>{this.state.product.nama}</strong>
                                            </h5>
                                        </Col>
                                        <Col md={{ span: 1 }}> </Col>
                                        <Col md={{ span: 3 }}></Col>
                                        <Col md={{ span: 3 }}></Col>
                                        <Col md={{ span: 1 }}> </Col>
                                        <Col md={{ span: 2 }}></Col>
                                    </Row>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Nama Produk <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Nama Produk"
                                                onChange={this.changeHandler}
                                                name="nama"
                                                type="text"
                                                value={this.state.nama}
                                                required
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Tanggal Akhir Izin
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Tanggal Akhir Izin"
                                                onChange={this.changeHandler}
                                                name="tgl_akhir_izin"
                                                type="date"
                                                value={this.state.tgl_akhir_izin}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            SKU <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="SKU"
                                                onChange={this.changeHandler}
                                                name="sku"
                                                type="text"
                                                value={this.state.sku}
                                                required
                                            />
                                        </Col>

                                        <Form.Label column sm={2}>
                                            Perusahaan <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Perusahaan"
                                                onChange={this.changeHandler}
                                                name="perusahaan"
                                                type="text"
                                                value={this.state.perusahaan}
                                                required
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Kategori
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                onChange={this.changeHandler}
                                                name="kategori"
                                                type="text"
                                                value={this.state.kategori}
                                            >
                                                <option selected>-- Pilih Kategori --</option>
                                                {renderCategorys}
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Ukuran <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Ukuran"
                                                onChange={this.changeHandler}
                                                name="ukuran"
                                                type="text"
                                                value={this.state.ukuran}
                                                required
                                            />
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Deskripsi
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="textarea"
                                                rows="4"
                                                onChange={this.changeHandler}
                                                name="deskripsi"
                                                type="text"
                                                value={this.state.deskripsi}
                                            ></Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <br></br>
                                    <Form.Group as={Row}>
                                        <Col sm={{ span: 7, offset: 5 }}>
                                            <Button variant="success" type="submit">
                                                Simpan Produk
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default EditProduk;
