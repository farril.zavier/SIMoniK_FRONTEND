import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";

import Modal from "react-bootstrap/Modal";

import classes from "./TambahProduk.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { api } from "../../../api";

class TambahProduk extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            successModalShow: false,
            failModalShow: false,
            categorys: [],
            nama: "",
            ukuran: "",
            sku: "",
            perusahaan: "",
            kategori: 0,
            tgl_akhir_izin: null,
            deskripsi: "",
        };
    }

    componentDidMount() {
        this.loadCategorys();
    }

    loadCategorys = async () => {
        const fetchedCategorys = [];
        const response = await api.get("kategori/");
        for (let key in response.data) {
            fetchedCategorys.push({
                ...response.data[key],
            });
        }
        this.setState({
            categorys: fetchedCategorys,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.reload();
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.addProducts();
    };

    async addProducts() {
        const product = {
            nama: this.state.nama,
            sku: this.state.sku,
            perusahaan: this.state.perusahaan,
            kategori: this.state.kategori,
            ukuran: this.state.ukuran,
            tgl_akhir_izin: this.state.tgl_akhir_izin,
            deskripsi: this.state.deskripsi,
        };
        try {
            const response = await api.post("produk/", product);
            if (!response.status === 201) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);

            console.log(product);
            this.setState({
                failModalShow: true,
            });
        }
    }

    changeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    render() {
        const renderCategorys = this.state.categorys.map((kategori, idx) => {
            return (
                <option key={idx} value={kategori.id}>
                    {kategori.nama}
                </option>
            );
        });

        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Produk berhasil dibuat!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content center text-center mb-3 mt-4">
                                Produk gagal dibuat!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/produk">Produk</Breadcrumb.Item>
                        <Breadcrumb.Item active>Tambah Produk</Breadcrumb.Item>
                    </Breadcrumb>
                    <Card>
                        <Card.Body>
                            <Card.Text>
                                <Card.Title>Tambah Produk</Card.Title>
                                <div className={classes.divider}></div>
                                <br></br>
                                <Form onSubmit={this.handleSubmit}>
                                    <Form.Group as={Row} controlId="formHorizontalNamaProduk">
                                        <Form.Label column sm={2}>
                                            Nama Produk
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                onChange={this.changeHandler}
                                                name="nama"
                                                type="text"
                                                placeholder="Nama Produk"
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="formHorizontalNamaProduk">
                                        <Form.Label column sm={2}>
                                            SKU
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                onChange={this.changeHandler}
                                                name="sku"
                                                type="number"
                                                placeholder="SKU Produk"
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="formHorizontalNamaProduk">
                                        <Form.Label column sm={2}>
                                            Ukuran Produk
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                onChange={this.changeHandler}
                                                name="ukuran"
                                                type="text"
                                                placeholder="Ukuran Produk"
                                            />
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="formGridKategori">
                                        <Form.Label column sm={2}>
                                            Kategori
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                onChange={this.changeHandler}
                                                name="kategori"
                                                as="select"
                                            >
                                                <option selected>-- Pilih Kategori --</option>
                                                {renderCategorys}
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="formHorizontalPerusahaan">
                                        <Form.Label column sm={2}>
                                            Perusahaan
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                onChange={this.changeHandler}
                                                name="perusahaan"
                                                type="text"
                                                placeholder="Perusahaan"
                                            />
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="formHorizontalTanggalAkhirIjin">
                                        <Form.Label column sm={2}>
                                            Tanggal Akhir Ijin
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                placeholder="Tanggal Akhir Izin"
                                                onChange={this.changeHandler}
                                                name="tgl_akhir_izin"
                                                type="date"
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="formHorizontalDeskripsiSingkat">
                                        <Form.Label column sm={2}>
                                            Deskripsi Singkat
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control
                                                as="textarea"
                                                rows={4}
                                                onChange={this.changeHandler}
                                                name="deskripsi"
                                                type="text"
                                                placeholder="Deskripsi Singkat"
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Col sm={{ span: 10, offset: 2 }}>
                                            <Button variant="success" type="submit">
                                                Simpan
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default TambahProduk;
