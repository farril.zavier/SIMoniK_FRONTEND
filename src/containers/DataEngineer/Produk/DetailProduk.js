import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Container from "react-bootstrap/Container";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";

import classes from "./DetailProduk.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { api } from "../../../api";

class DetailProduk extends Component {
    constructor(props) {
        super(props);

        this.state = {
            idProduk: props.match.params.id,
            produk: {},
            confirmDeleteModalShow: false,
            successModalShow: false,
            failModalShow: false,
        };
    }

    modalShowHandler(accountId) {
        this.setState({
            modalShow: true,
            currentId: accountId,
        });
    }

    modalCloseHandler = () => {
        this.setState({
            modalShow: false,
            currentId: undefined,
        });
    };

    componentDidMount() {
        this.loadProducts();
    }

    loadProducts = async () => {
        let idProduk = this.state.idProduk;
        const response = await api.get("produk/" + idProduk + "/");
        const produk = response.data;
        this.setState({
            produk: produk,
        });
    };

    deleteDepo = async () => {
        const { idProduk } = this.state;
        this.confirmDeleteModalCloseHandler();
        try {
            const response = await api.del("produk/" + idProduk + "/");
            if (!response.status === 200 || !response.status === 204) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({ successModalShow: true });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    };

    confirmDeleteModalShowHandler = () => {
        this.setState({
            confirmDeleteModalShow: true,
        });
    };

    confirmDeleteModalCloseHandler = () => {
        this.setState({
            confirmDeleteModalShow: false,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                const currentUrl = window.location.pathname;
                const redirectUrl = currentUrl.slice(0, -9);
                window.location.replace(redirectUrl);
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    render() {
        const { produk } = this.state;

        return (
            <React.Fragment>
                <Content>
                    <div className={classes.breadCrumbCurrent}>
                        <Breadcrumb className={classes.breadcrumbBackground}>
                            <Breadcrumb.Item href="/produk">Produk</Breadcrumb.Item>
                            <Breadcrumb.Item active>{produk.nama}</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>

                    <Card>
                        <Modal
                            centered
                            show={this.state.confirmDeleteModalShow}
                            onHide={this.confirmDeleteModalCloseHandler}
                        >
                            <Modal.Header
                                closeButton
                                className={classes.modalHeader}
                            ></Modal.Header>
                            <Modal.Body className="mt-n5">
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    Apakah Anda yakin ingin menghapus produk ini?
                                </Row>
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    <Button
                                        className="mx-2"
                                        onClick={this.deleteDepo}
                                        variant="light"
                                    >
                                        Ya, Hapus
                                    </Button>
                                    <Button
                                        className="mx-2"
                                        onClick={this.confirmDeleteModalCloseHandler}
                                        variant="primary"
                                    >
                                        Tidak
                                    </Button>
                                </Row>
                            </Modal.Body>
                        </Modal>
                        <Modal
                            centered
                            show={this.state.successModalShow}
                            onHide={this.successModalCloseHandler}
                        >
                            <Modal.Header
                                closeButton
                                className={classes.modalHeader}
                            ></Modal.Header>
                            <Modal.Body className="mt-n5">
                                <Row className="justify-content-center mt-2 mb-4">
                                    <img src={require("../../../assets/check_circle.svg")} />
                                </Row>
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    Produk berhasil dihapus!
                                </Row>
                            </Modal.Body>
                        </Modal>
                        <Modal
                            centered
                            show={this.state.failModalShow}
                            onHide={this.failModalCloseHandler}
                        >
                            <Modal.Header
                                closeButton
                                className={classes.modalHeader}
                            ></Modal.Header>
                            <Modal.Body className="mt-n5">
                                <Row className="justify-content-center mt-2 mb-4">
                                    <img src={require("../../../assets/x_circle.svg")} />
                                </Row>
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    Produk gagal dihapus!
                                </Row>
                            </Modal.Body>
                        </Modal>
                        <Card.Body>
                            <Card.Text>
                                <h4>Produk</h4>
                                <div className={classes.divider}></div>
                                <h5>
                                    <center>Data Produk</center>
                                </h5>
                                <div className={classes.divider}></div>
                                <Container>
                                    <Row className="my-4">
                                        <Col md={{ span: 6 }}>
                                            <h5>
                                                <strong>{produk.nama}</strong>
                                            </h5>
                                        </Col>
                                        <Col md={{ span: 6 }}> </Col>
                                    </Row>

                                    <Row>
                                        <Col md={{ span: 2 }}>ID Produk</Col>
                                        <Col md={{ span: 3, offset: 1 }}>
                                            : {this.state.idProduk}
                                        </Col>
                                        <Col md={{ span: 2 }}>Tanggal Akhir Ijin</Col>
                                        <Col md={{ span: 3, offset: 1 }}>
                                            : {produk.tgl_akhir_izin}
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={{ span: 2 }}>SKU</Col>
                                        <Col md={{ span: 3, offset: 1 }}>: {produk.sku}</Col>
                                        <Col md={{ span: 2 }}>Perusahaan</Col>
                                        <Col md={{ span: 3, offset: 1 }}>: {produk.perusahaan}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>Kategori</Col>
                                        <Col md={{ span: 3, offset: 1 }}>
                                            : {produk.kategori_nama}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>Ukuran</Col>
                                        <Col md={{ span: 3, offset: 1 }}>: {produk.ukuran}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>Deskripsi</Col>
                                        <Col md={{ span: 3, offset: 1 }}>: {produk.deskripsi}</Col>
                                    </Row>

                                    <ButtonToolbar className="mt-4">
                                        <Row>
                                            <Button
                                                onClick={this.confirmDeleteModalShowHandler}
                                                className="mx-3"
                                                variant="danger"
                                            >
                                                Hapus Produk
                                            </Button>
                                        </Row>
                                    </ButtonToolbar>
                                </Container>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DetailProduk;
