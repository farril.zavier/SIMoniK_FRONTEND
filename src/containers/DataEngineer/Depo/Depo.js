import React from "react";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";

import classes from "./Depo.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

const Depo = (props) => {
    return (
        <tr>
            <td>{props.idx}</td>
            {/* <td>{props.sku}</td> */}
            <td>{props.nama}</td>
            {/* <td>{props.ukuran}</td> */}
            <td>{props.kodeToko}</td>
            <td>{props.kabupaten}</td>
            {/* <td>{props.tgl_akhir_izin}</td> */}
            {/* <td>{props.deskripsi}</td> */}
            <td>
                <Button href={"/depo/detail/" + props.id} variant="primary">
                    Lihat Detail
                </Button>
            </td>
            <td>
                <Button href={"/depo/ubah/" + props.id} variant="secondary">
                    Ubah Detail Depo
                </Button>
            </td>
        </tr>
    );
};

export default Depo;
