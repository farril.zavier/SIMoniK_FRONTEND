import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

import classes from "./EditDepo.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { api } from "../../../api";

class EditDepo extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            successModalShow: false,
            failModalShow: false,
            idDepo: props.match.params.id,
            depo: {},
            kabupatens: [],
            kecamatans: [],
            desas: [],
            allFieldOfficers: [],
            fieldOfficers: [],
            kode_toko: "",
            nama: "",
            status: "",
            kabupaten: "0",
            kecamatan: "0",
            desa: "0",
            titik_lokasi: "",
            alamat_lengkap: "",
            keterangan: "",
            pelatihan: "",
            field_officer: "0",
            surat_kontrak: "",
            tgl_surat_kontrak: null,
            kp: "",
            buku_transaksi: "",
            ukuran_spanduk: "",
            keterangan_spanduk: "",
            pencatatan: "",
            omset: null,
            nama_pemilik: "",
            nik_pemilik: "",
            no_hp: "",
            nama_pasangan: "",
            nik_pasangan: "",
        };
    }

    componentDidMount() {
        this.loadKabupatens();
        // this.loadDepo();
        // this.loadFieldOfficers();
        // this.loadDesas();
        // this.loadKecamatans();
    }

    loadKabupatens = async () => {
        const fetchedKabupatens = [];
        const response = await api.get("depo/form-kabupaten/");
        for (let key in response.data) {
            fetchedKabupatens.push({
                ...response.data[key],
            });
        }
        this.setState(
            {
                kabupatens: fetchedKabupatens,
            },
            () => {
                this.loadDepo();
            }
        );
    };

    loadDepo = async () => {
        let idDepo = this.state.idDepo;
        const response = await api.get("depo/depo/" + idDepo + "/");
        const depo = response.data;
        this.setState(
            {
                depo: depo,
                kode_toko: depo.kode_toko,
                nama: depo.nama,
                status: depo.status,
                kabupaten: depo.desa.kecamatan.kabupaten.id.toString(),
                kecamatan: depo.desa.kecamatan.id.toString(),
                desa: depo.desa.id.toString(),
                titik_lokasi: depo.titik_lokasi,
                alamat_lengkap: depo.alamat_lengkap,
                keterangan: depo.keterangan,
                pelatihan: depo.pelatihan,
                field_officer: depo.field_officer.id.toString(),
                surat_kontrak: depo.surat_kontrak,
                tgl_surat_kontrak: depo.tgl_surat_kontrak,
                kp: depo.kp,
                buku_transaksi: depo.buku_transaksi,
                ukuran_spanduk: depo.ukuran_spanduk,
                keterangan_spanduk: depo.keterangan_spanduk,
                pencatatan: depo.pencatatan,
                omset: depo.omset,
                nama_pemilik: depo.nama_pemilik,
                nik_pemilik: depo.nik_pemilik,
                no_hp: depo.no_hp,
                nama_pasangan: depo.nama_pasangan,
                nik_pasangan: depo.nik_pasangan,
            },
            () => {
                this.loadFieldOfficers();
            }
        );
    };

    loadFieldOfficers = async () => {
        const fetchedFieldOfficers = [];
        const response = await api.get("depo/field-officers/");
        for (let key in response.data) {
            fetchedFieldOfficers.push({
                ...response.data[key],
            });
        }
        this.setState(
            {
                allFieldOfficers: fetchedFieldOfficers,
            },
            () => {
                let fieldOfficers = [];
                for (const fieldOfficer of this.state.allFieldOfficers) {
                    if (fieldOfficer.kabupaten.id === Number(this.state.kabupaten)) {
                        fieldOfficers.push(fieldOfficer);
                    }
                }
                this.setState({
                    fieldOfficers: fieldOfficers,
                });
            }
        );
    };

    // loadKecamatans = async () => {
    //     const fetchedKecamatans = [];
    //     const response = await api.get("depo/kecamatan/");
    //     for (let key in response.data) {
    //         fetchedKecamatans.push({
    //             ...response.data[key],
    //         });
    //     }
    //     this.setState({
    //         kecamatans: fetchedKecamatans,
    //     });
    // };

    // loadDesas = async () => {
    //     const fetchedDesas = [];
    //     const response = await api.get("depo/desa/");
    //     for (let key in response.data) {
    //         fetchedDesas.push({
    //             ...response.data[key],
    //         });
    //     }
    //     this.setState({
    //         desas: fetchedDesas,
    //     });
    // };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.reload();
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.editDepo();
    };

    async editDepo() {
        const depo = {
            kode_toko: this.state.kode_toko,
            nama: this.state.nama,
            status: this.state.status,
            desa: this.state.desa,
            titik_lokasi: this.state.titik_lokasi,
            alamat_lengkap: this.state.alamat_lengkap,
            keterangan: this.state.keterangan,
            pelatihan: this.state.pelatihan,
            field_officer: this.state.field_officer,
            surat_kontrak: this.state.surat_kontrak,
            tgl_surat_kontrak: this.state.tgl_surat_kontrak,
            kp: this.state.kp,
            buku_transaksi: this.state.buku_transaksi,
            ukuran_spanduk: this.state.ukuran_spanduk,
            keterangan_spanduk: this.state.keterangan_spanduk,
            pencatatan: this.state.pencatatan,
            omset: this.state.omset,
            nama_pemilik: this.state.nama_pemilik,
            nik_pemilik: this.state.nik_pemilik,
            no_hp: this.state.no_hp,
            nama_pasangan: this.state.nama_pasangan,
            nik_pasangan: this.state.nik_pasangan,
        };
        try {
            let idDepo = this.state.idDepo;
            const response = await api.put("depo/edit-depo/" + idDepo + "/", depo);
            if (!response.status === 200) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    changeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    kabupatenChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState(
            {
                [name]: value,
                kecamatan: "0",
                desa: "0",
                field_officer: "0",
            },
            () => {
                let fieldOfficers = [];
                for (const fieldOfficer of this.state.allFieldOfficers) {
                    if (fieldOfficer.kabupaten.id === Number(this.state.kabupaten)) {
                        fieldOfficers.push(fieldOfficer);
                    }
                }
                this.setState({
                    fieldOfficers: fieldOfficers,
                });
            }
        );
    };

    kecamatanChangeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            desa: "0",
        });
    };

    getObject(val, arr, prop) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][prop] === val) {
                return arr[i];
            }
        }
        return -1;
    }

    render() {
        const renderKabupatens = this.state.kabupatens.map((kabupaten, idx) => {
            return (
                <option key={idx} value={kabupaten.id}>
                    {kabupaten.nama}
                </option>
            );
        });
        let renderFormKecamatan;
        if (this.state.kabupaten !== "0") {
            const kabupaten = this.getObject(
                Number(this.state.kabupaten),
                this.state.kabupatens,
                "id"
            );
            if (kabupaten !== -1) {
                const renderKecamatans = kabupaten.daftar_kecamatan.map((kecamatan, idx) => {
                    return (
                        <option key={idx} value={kecamatan.id}>
                            {kecamatan.nama}
                        </option>
                    );
                });
                renderFormKecamatan = (
                    <React.Fragment>
                        <Form.Label column sm={2}>
                            Kecamatan <font color="red">*</font>
                        </Form.Label>
                        <Col sm={4}>
                            <Form.Control
                                as="select"
                                placeholder="Kecamatan"
                                onChange={this.kecamatanChangeHandler}
                                name="kecamatan"
                                type="text"
                                value={this.state.kecamatan}
                                required
                            >
                                <option value={0} selected>
                                    -- Pilih Kecamatan --
                                </option>
                                {renderKecamatans}
                            </Form.Control>
                        </Col>
                    </React.Fragment>
                );
            }
        } else {
            renderFormKecamatan = (
                <React.Fragment>
                    <Form.Label column sm={2}></Form.Label>
                    <Col sm={4}></Col>
                </React.Fragment>
            );
        }
        let renderFormDesa;
        if (this.state.kecamatan !== "0" && this.state.kabupaten !== "0") {
            const kabupaten = this.getObject(
                Number(this.state.kabupaten),
                this.state.kabupatens,
                "id"
            );
            const kecamatan = this.getObject(
                Number(this.state.kecamatan),
                kabupaten.daftar_kecamatan,
                "id"
            );
            if (kecamatan !== -1) {
                const renderDesas = kecamatan.daftar_desa.map((desa, idx) => {
                    return (
                        <option key={idx} value={desa.id}>
                            {desa.nama}
                        </option>
                    );
                });
                renderFormDesa = (
                    <React.Fragment>
                        <Form.Label column sm={2}>
                            Desa <font color="red">*</font>
                        </Form.Label>
                        <Col sm={4}>
                            <Form.Control
                                as="select"
                                placeholder="Desa"
                                onChange={this.changeHandler}
                                name="desa"
                                type="text"
                                value={this.state.desa}
                                required
                            >
                                <option value={0} selected>
                                    -- Pilih Desa --
                                </option>
                                {renderDesas}
                            </Form.Control>
                        </Col>
                    </React.Fragment>
                );
            }
        } else {
            renderFormDesa = (
                <React.Fragment>
                    <Form.Label column sm={2}></Form.Label>
                    <Col sm={4}></Col>
                </React.Fragment>
            );
        }

        let renderFormFieldOfficer;
        if (this.state.kabupaten !== "0") {
            const renderFieldOfficers = this.state.fieldOfficers.map((fieldOfficer, idx) => {
                return (
                    <option key={idx} value={fieldOfficer.id}>
                        {fieldOfficer.nama}
                    </option>
                );
            });
            renderFormFieldOfficer = (
                <React.Fragment>
                    <Form.Label column sm={2}>
                        Field Officer <font color="red">*</font>
                    </Form.Label>
                    <Col sm={4}>
                        <Form.Control
                            as="select"
                            placeholder="Field Officer"
                            onChange={this.changeHandler}
                            name="field_officer"
                            type="text"
                            value={this.state.field_officer}
                            required
                        >
                            <option selected>-- Pilih Field Officer --</option>
                            {renderFieldOfficers}
                        </Form.Control>
                    </Col>
                </React.Fragment>
            );
        } else {
            renderFormFieldOfficer = (
                <React.Fragment>
                    <Form.Label column sm={2}></Form.Label>
                    <Col sm={4}></Col>
                </React.Fragment>
            );
        }
        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Depo berhasil diubah!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Depo gagal diubah!
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/depo">Depo</Breadcrumb.Item>
                        <Breadcrumb.Item active>Ubah Detail Depo</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Text>
                                <h4>Depo</h4>
                                <div className={classes.divider}></div>
                                <h5>
                                    <center>Data Depo</center>
                                </h5>
                                <div className={classes.divider}></div>
                                <Form onSubmit={this.handleSubmit}>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <h5>
                                                <strong>Identitas</strong>
                                            </h5>
                                        </Col>
                                        <Col md={{ span: 1 }}> </Col>
                                        <Col md={{ span: 3 }}></Col>
                                        <Col md={{ span: 3 }}>
                                            <h5>
                                                <strong>Lain-lain</strong>
                                            </h5>
                                        </Col>
                                        <Col md={{ span: 1 }}> </Col>
                                        <Col md={{ span: 2 }}></Col>
                                    </Row>

                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Kode Depo <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Kode Depo"
                                                onChange={this.changeHandler}
                                                name="kode_toko"
                                                type="text"
                                                value={this.state.kode_toko}
                                                required
                                            />
                                        </Col>
                                        {renderFormFieldOfficer}
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Nama Depo <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Nama Depo"
                                                onChange={this.changeHandler}
                                                name="nama"
                                                type="text"
                                                value={this.state.nama}
                                                required
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Surat Kontrak
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                onChange={this.changeHandler}
                                                name="surat_kontrak"
                                                type="text"
                                                value={this.state.surat_kontrak}
                                            >
                                                <option selected>Status Surat...</option>
                                                <option value="SUDAH">SUDAH</option>
                                                <option value="BELUM">BELUM</option>
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Status Depo
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                onChange={this.changeHandler}
                                                name="status"
                                                type="text"
                                                value={this.state.status}
                                            >
                                                <option selected>Status</option>
                                                <option value="OK">OK</option>
                                                <option value="CANCEL">CANCEL</option>
                                                <option value="FOLLOW UP">FOLLOW UP</option>
                                            </Form.Control>
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Tanggal Surat Kontrak
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Tanggal Surat Kontrak"
                                                onChange={this.changeHandler}
                                                name="tgl_surat_kontrak"
                                                type="date"
                                                value={this.state.tgl_surat_kontrak}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Kabupaten <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                placeholder="Kabupaten"
                                                onChange={this.kabupatenChangeHandler}
                                                name="kabupaten"
                                                type="text"
                                                value={this.state.kabupaten}
                                                required
                                            >
                                                <option value={0} selected>
                                                    -- Pilih Kabupaten --
                                                </option>
                                                {renderKabupatens}
                                            </Form.Control>
                                        </Col>
                                        <Form.Label column sm={2}>
                                            KP
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="KP"
                                                onChange={this.changeHandler}
                                                name="kp"
                                                type="text"
                                                value={this.state.kp}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        {renderFormKecamatan}
                                        <Form.Label column sm={2}>
                                            Buku Transaksi
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                onChange={this.changeHandler}
                                                name="buku_transaksi"
                                                type="text"
                                                value={this.state.buku_transaksi}
                                            >
                                                <option selected>Status Buku Transaksi...</option>
                                                <option value="SUDAH">SUDAH</option>
                                                <option value="BELUM">BELUM</option>
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        {renderFormDesa}
                                        <Form.Label column sm={2}>
                                            Ukuran Spanduk
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Ukuran Spanduk"
                                                onChange={this.changeHandler}
                                                name="ukuran_spanduk"
                                                type="text"
                                                value={this.state.ukuran_spanduk}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Titik Lokasi
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Titik Lokasi"
                                                onChange={this.changeHandler}
                                                name="titik_lokasi"
                                                type="text"
                                                value={this.state.titik_lokasi}
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Keterangan Spanduk
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Keterangan Spanduk"
                                                onChange={this.changeHandler}
                                                name="keterangan_spanduk"
                                                type="text"
                                                value={this.state.keterangan_spanduk}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Alamat
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Alamat"
                                                onChange={this.changeHandler}
                                                name="alamat_lengkap"
                                                type="text"
                                                value={this.state.alamat_lengkap}
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Pencatatan
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                onChange={this.changeHandler}
                                                name="pencatatan"
                                                type="text"
                                                value={this.state.pencatatan}
                                            >
                                                <option selected>Status Pencatatan...</option>
                                                <option value="SUDAH RAPI">SUDAH RAPI</option>
                                                <option value="SUDAH ADA">SUDAH ADA</option>
                                                <option value="BELUM RAPI">BELUM RAPI</option>
                                                <option value="BELUM ADA">BELUM ADA</option>
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Keterangan
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Keterangan"
                                                onChange={this.changeHandler}
                                                name="keterangan"
                                                type="text"
                                                value={this.state.keterangan}
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Omset (per bulan)
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Omset per bulan"
                                                onChange={this.changeHandler}
                                                name="omset"
                                                type="number"
                                                value={this.state.omset}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Pelatihan 1
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                as="select"
                                                onChange={this.changeHandler}
                                                name="pelatihan"
                                                type="text"
                                                value={this.state.pelatihan}
                                            >
                                                <option selected>Pelatihan 1</option>
                                                <option value="SUDAH">SUDAH</option>
                                                <option value="BELUM">BELUM</option>
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <br></br>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <h5>
                                                <strong>Pemilik</strong>
                                            </h5>
                                        </Col>
                                    </Row>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Nama Pemilik <font color="red">*</font>
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Nama Pemilik"
                                                onChange={this.changeHandler}
                                                name="nama_pemilik"
                                                type="text"
                                                value={this.state.nama_pemilik}
                                                required
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            Nama Pasangan
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Nama Pasangan"
                                                onChange={this.changeHandler}
                                                name="nama_pasangan"
                                                type="text"
                                                value={this.state.nama_pasangan}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            NIK Pemilik
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="NIK Pemilik"
                                                onChange={this.changeHandler}
                                                name="nik_pemilik"
                                                type="number"
                                                value={this.state.nik_pemilik}
                                            />
                                        </Col>
                                        <Form.Label column sm={2}>
                                            NIK Pasangan
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="NIK Pasangan"
                                                onChange={this.changeHandler}
                                                name="nik_pasangan"
                                                type="number"
                                                value={this.state.nik_pasangan}
                                            />
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Nomor HP
                                        </Form.Label>
                                        <Col sm={4}>
                                            <Form.Control
                                                placeholder="Nomor HP"
                                                onChange={this.changeHandler}
                                                name="no_hp"
                                                type="number"
                                                value={this.state.no_hp}
                                            />
                                        </Col>
                                    </Form.Group>
                                    <br></br>
                                    <Form.Group as={Row}>
                                        <Col sm={{ span: 7, offset: 5 }}>
                                            <Button variant="success" type="submit">
                                                Simpan Depo
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default EditDepo;
