import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Container from "react-bootstrap/Container";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";

import classes from "./DetailDepo.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { api } from "../../../api";

class DetailDepo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            idDepo: props.match.params.id,
            depo: {},
            confirmDeleteModalShow: false,
            successModalShow: false,
            failModalShow: false,
        };
    }

    componentDidMount() {
        this.loadDepo();
    }

    loadDepo = async () => {
        let idDepo = this.state.idDepo;
        const response = await api.get("depo/depo/" + idDepo + "/");
        const depo = response.data;
        this.setState({
            depo: depo,
        });
    };

    deleteDepo = async () => {
        const { idDepo } = this.state;
        this.confirmDeleteModalCloseHandler();
        try {
            const response = await api.del("depo/depo/" + idDepo + "/");
            if (!response.status === 200 || !response.status === 204) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({ successModalShow: true });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    };

    confirmDeleteModalShowHandler = () => {
        this.setState({
            confirmDeleteModalShow: true,
        });
    };

    confirmDeleteModalCloseHandler = () => {
        this.setState({
            confirmDeleteModalShow: false,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                const currentUrl = window.location.pathname;
                const redirectUrl = currentUrl.slice(0, -9);
                window.location.replace(redirectUrl);
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    render() {
        let stock_opnames = [];
        if (this.state.depo.stock_opnames !== undefined) {
            stock_opnames = this.state.depo.stock_opnames.map((stock_opname, idx) => (
                <tr>
                    <td>{idx + 1}</td>
                    <td>{stock_opname.id}</td>
                    <td>{stock_opname.status}</td>
                    <td>{stock_opname.field_officer}</td>
                    <td>{stock_opname.tgl_mulai}</td>
                    <td>{new Date(stock_opname.waktu_submit).toLocaleString()}</td>
                </tr>
            ));
        }
        const { depo } = this.state;
        return (
            <React.Fragment>
                <Content>
                    <div className={classes.breadCrumbCurrent}>
                        <Breadcrumb className={classes.breadcrumbBackground}>
                            <Breadcrumb.Item href="/depo">Depo</Breadcrumb.Item>
                            <Breadcrumb.Item active>{depo.nama}</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>

                    <Card>
                        <Modal
                            centered
                            show={this.state.confirmDeleteModalShow}
                            onHide={this.confirmDeleteModalCloseHandler}
                        >
                            <Modal.Header
                                closeButton
                                className={classes.modalHeader}
                            ></Modal.Header>
                            <Modal.Body className="mt-n5">
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    Apakah Anda yakin ingin menghapus depo ini?
                                </Row>
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    <Button
                                        className="mx-2"
                                        onClick={this.deleteDepo}
                                        variant="primary"
                                    >
                                        Ya, Hapus
                                    </Button>
                                    <Button
                                        className="mx-2"
                                        onClick={this.confirmDeleteModalCloseHandler}
                                        variant="light"
                                    >
                                        Tidak
                                    </Button>
                                </Row>
                            </Modal.Body>
                        </Modal>
                        <Modal
                            centered
                            show={this.state.successModalShow}
                            onHide={this.successModalCloseHandler}
                        >
                            <Modal.Header
                                closeButton
                                className={classes.modalHeader}
                            ></Modal.Header>
                            <Modal.Body className="mt-n5">
                                <Row className="justify-content-center mt-2 mb-4">
                                    <img src={require("../../../assets/check_circle.svg")} />
                                </Row>
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    Depo berhasil dihapus!
                                </Row>
                            </Modal.Body>
                        </Modal>
                        <Modal
                            centered
                            show={this.state.failModalShow}
                            onHide={this.failModalCloseHandler}
                        >
                            <Modal.Header
                                closeButton
                                className={classes.modalHeader}
                            ></Modal.Header>
                            <Modal.Body className="mt-n5">
                                <Row className="justify-content-center mt-2 mb-4">
                                    <img src={require("../../../assets/x_circle.svg")} />
                                </Row>
                                <Row className="justify-content-center text-center mb-3 mt-4">
                                    Depo gagal dihapus!
                                </Row>
                            </Modal.Body>
                        </Modal>
                        <Card.Body>
                            <Card.Text>
                                <h4>Depo</h4>
                                <div className={classes.divider}></div>
                                <h5>
                                    <center>Data Depo</center>
                                </h5>
                                <div className={classes.divider}></div>
                                <Container>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <h5>
                                                <strong>Identitas</strong>
                                            </h5>
                                        </Col>
                                        <Col md={{ span: 1 }}> </Col>
                                        <Col md={{ span: 3 }}></Col>
                                        <Col md={{ span: 3 }}>
                                            <h5>
                                                <strong>Lain-lain</strong>
                                            </h5>
                                        </Col>
                                        <Col md={{ span: 1 }}> </Col>
                                        <Col md={{ span: 2 }}></Col>
                                    </Row>

                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>ID Depo</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{this.state.idDepo}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Omset per bulan</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.omset}</Col>
                                    </Row>

                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Kode Depo</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.kode_toko}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Pencatatan</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.pencatatan}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Nama Depo</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.nama}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Surat Kontrak</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.surat_kontrak}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Status Depo</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.status}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Tanggal Surat Kontrak</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.tgl_surat_kontrak}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Kabupaten</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>
                                            {depo.desa && depo.desa.kecamatan.kabupaten.nama}
                                        </Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>KP (Kader Pertanian)</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.kp}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Kecamatan</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>
                                            {depo.desa && depo.desa.kecamatan.nama}
                                        </Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Buku Transaksi</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.buku_transaksi}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Desa</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.desa && depo.desa.nama}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Ukuran Spanduk</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.ukuran_spanduk}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Titik Lokasi</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.titik_lokasi}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Keterangan Spanduk</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.keterangan_spanduk}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Alamat</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.alamat_lengkap}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Kategori Produk</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{/* TODO */}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Pelatihan 1</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.pelatihan}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Field Officer</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>
                                            {depo.field_officer && depo.field_officer.nama}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Keterangan</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.keterangan}</Col>
                                    </Row>
                                    <br></br>
                                    <br></br>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>
                                                <h5>
                                                    <strong>Pemilik</strong>
                                                </h5>
                                            </strong>
                                        </Col>
                                        <Col md={{ span: 1 }}></Col>
                                        <Col md={{ span: 3 }}></Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Nama Pemilik</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.nama_pemilik}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>Nama Pasangan</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.nama_pasangan}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>NIK Pemilik</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.nik_pemilik}</Col>
                                        <Col md={{ span: 3 }}>
                                            <strong>NIK Pasangan</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 2 }}>{depo.nik_pasangan}</Col>
                                    </Row>
                                    <Row>
                                        <Col md={{ span: 2 }}>
                                            <strong>Nomor HP</strong>
                                        </Col>
                                        <Col md={{ span: 1 }}>: </Col>
                                        <Col md={{ span: 3 }}>{depo.no_hp}</Col>
                                    </Row>

                                    <br></br>

                                    <ButtonToolbar>
                                        <Row>
                                            <Button
                                                onClick={this.confirmDeleteModalShowHandler}
                                                className="mx-3"
                                                variant="danger"
                                            >
                                                Hapus Depo
                                            </Button>
                                        </Row>
                                    </ButtonToolbar>
                                </Container>
                                <br></br>
                                <h5>
                                    <center>Histori Stock Opname</center>
                                </h5>
                                <div className={classes.divider}></div>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Stock Opname</th>
                                            <th>Status</th>
                                            <th>Field Officer</th>
                                            <th>Waktu Pencatatan SO</th>
                                            <th>Waktu Upload SO</th>
                                        </tr>
                                    </thead>
                                    <tbody>{stock_opnames}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DetailDepo;
