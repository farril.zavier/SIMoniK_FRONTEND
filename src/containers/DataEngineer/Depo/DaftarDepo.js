import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card, { CardBody, CardTitle } from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form, { FormRow } from "react-bootstrap/Form";
import { api } from "../../../api";

import classes from "./DaftarDepo.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Depo from "./Depo";

class DaftarDepo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            depos: [],
            kabupatens: [],
            currentId: undefined,
            filter: 0,
            search: 0,
            nama: 0,
            kabupaten: 0,
        };
    }

    componentDidMount() {
        this.loadDepos();
        this.loadKabupatens();
    }

    loadDepos = async () => {
        const fetchedDepos = [];
        const response = await api.get("depo/depo/");
        for (let key in response.data) {
            fetchedDepos.push({
                ...response.data[key],
            });
        }
        this.setState({
            depos: fetchedDepos,
        });
    };

    loadKabupatens = async () => {
        const fetchedKabupatens = [];
        const response = await api.get("depo/kabupaten/");
        for (let key in response.data) {
            fetchedKabupatens.push({
                ...response.data[key],
            });
        }
        this.setState({
            kabupatens: fetchedKabupatens,
        });
    };

    filterHandler = (event) => {
        const { name, value } = event.target;
        console.log(name + value);
        this.setState({
            [name]: value,
        });
    };

    searchHandler = (event) => {
        const { name, value } = event.target;
        console.log(name + value);
        this.setState({
            [name]: value,
        });
    };

    submitHandler = (event) => {
        event.preventDefault();
        this.setState({
            filter: this.state.kabupaten,
            search: this.state.nama,
        });
    };

    render() {
        var testdepos = this.state.depos.filter(
            (depo, idx) => this.state.filter == 0 || depo.kabupaten == this.state.filter
        );
        const renderDepos = this.state.depos
            .filter(
                (depo, idx) =>
                    (this.state.filter == 0 ||
                        depo.desa.kecamatan.kabupaten.id.toString() == this.state.filter) &&
                    (this.state.search == 0 ||
                        depo.nama.toLowerCase() == this.state.search.toLowerCase())
            )
            .map((depo, idx) => (
                <Depo
                    key={idx}
                    idx={idx + 1}
                    nama={depo.nama}
                    kodeToko={depo.kode_toko}
                    kabupaten={depo.desa.kecamatan.kabupaten.nama}
                    id={depo.id}
                />
            ));

        const renderKabupatens = this.state.kabupatens.map((kabupaten, idx) => {
            return (
                <option key={idx} value={kabupaten.id}>
                    {kabupaten.nama}
                </option>
            );
        });

        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Depo</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <h5>Depo</h5>
                            <div className={classes.divider}></div>
                            <Button href="/depo/tambah" variant="outline-primary">
                                + Tambah Depo
                            </Button>
                            <div className={classes.divider}></div>

                            <Card.Text>
                                <h6>Cari Berdasarkan</h6>
                                <Form inline onSubmit={this.submitHandler}>
                                    <Form.Control
                                        onChange={this.filterHandler}
                                        name="kabupaten"
                                        as="select"
                                    >
                                        <option selected value={0}>
                                            -- Semua Kabupaten --
                                        </option>
                                        {renderKabupatens}
                                    </Form.Control>
                                    <Form.Control
                                        onChange={this.searchHandler}
                                        name="nama"
                                        type="text"
                                        className="ml-3"
                                        placeholder="-- Nama Depo --"
                                    />
                                    <Button
                                        className="ml-3"
                                        column
                                        sm={2}
                                        variant="primary"
                                        type="submit"
                                    >
                                        Cari
                                    </Button>
                                </Form>

                                <br></br>
                                <h5> Semua Depo</h5>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Depo</th>
                                            <th>Kode Depo</th>
                                            <th>Kabupaten</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderDepos}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarDepo;
