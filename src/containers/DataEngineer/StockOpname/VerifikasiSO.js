import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { api } from "../../../api";

import classes from "./VerifikasiSO.module.css";

class VerifikasiSO extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stockOpnames: [],
            filterModalShow: false,
            namaDokumen: "",
            namaDepo: "",
        };
    }

    componentWillMount() {
        this.loadStockOpnames();
    }

    loadStockOpnames = async () => {
        const fetchedStockOpnames = [];
        const response = await api.get("stockopname/stock-opname-list/");
        for (let key in response.data) {
            fetchedStockOpnames.push({
                ...response.data[key],
            });
        }
        this.setState({
            stockOpnames: fetchedStockOpnames,
        });
    };

    filterButtonClickHandler = (event) => {
        event.preventDefault();
        this.setState({
            filterModalShow: true,
        });
    };

    filterModalCloseHandler = () => {
        this.setState({
            filterModalShow: false,
        });
    };

    render() {
        const rows = this.state.stockOpnames.map((stockOpname, idx) => (
            <tr>
                <td>{idx + 1}</td>
                <td>
                    {"Stock Opname " +
                        stockOpname.no_so +
                        " Bln. " +
                        new Date(stockOpname.submission.waktu_submit).toLocaleString("default", {
                            month: "long",
                        })}
                </td>
                <td>{stockOpname.depo.nama}</td>
                <td>{stockOpname.depo.daerah}</td>
                <td>
                    {stockOpname.status === 0 || stockOpname.status === null ? (
                        <span style={{ color: "red" }}>BELUM VERIFIKASI</span>
                    ) : (
                        <span style={{ color: "green" }}>TERVERIFIKASI</span>
                    )}
                </td>
                <td>{new Date(stockOpname.submission.waktu_submit).toLocaleString()}</td>
                <td>
                    <Button variant="primary" href={"/verifikasi-so/detail/" + stockOpname.id}>
                        Lihat Detail
                    </Button>
                </td>
            </tr>
        ));

        let renderPeriodeOptions;
        if (this.state.dateOptions === "periode") {
            renderPeriodeOptions = (
                <React.Fragment>
                    <option value="-1">Pilih periode ...</option>
                    <option value="0">3 bulan</option>
                    <option value="1">6 bulan</option>
                    <option value="2">12 bulan</option>
                </React.Fragment>
            );
        } else {
            renderPeriodeOptions = (
                <option value="-1" selected>
                    Pilih periode ...
                </option>
            );
        }
        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.filterModalShow}
                        onHide={this.filterModalCloseHandler}
                    >
                        <Modal.Header closeButton>Filter berdasarkan</Modal.Header>
                        <Modal.Body>
                            <Form>
                                <Form.Label>Waktu</Form.Label>
                                <Form.Group as={Row} controlId="formFilterRow1">
                                    <Col sm="6">
                                        <Form.Check
                                            inline
                                            type="radio"
                                            aria-label="Periode"
                                            name="dateOptions"
                                            value="periode"
                                            className="mr-1"
                                            onChange={this.radioButtonChangeHandler}
                                        />
                                        Periode
                                        <Form.Group
                                            as={Row}
                                            controlId="formPeriode"
                                            className="mt-2"
                                        >
                                            <Col sm="11" className="ml-auto">
                                                <Form.Control
                                                    as="select"
                                                    onChange={this.periodeChangeHandler}
                                                    name="periode"
                                                    type="text"
                                                    value={this.state.periode}
                                                >
                                                    {renderPeriodeOptions}
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm="6">
                                        <Form.Group>
                                            <Form.Label>Status Stock Opname</Form.Label>
                                            <Form.Control
                                                as="select"
                                                onChange={this.searchKeyChangeHandler}
                                                name="searchKey"
                                                type="text"
                                                value={this.state.searchKey}
                                            >
                                                <option selected>-- Pilih Penyaring --</option>
                                                <option value="0">Kategori</option>
                                                <option value="1">Daerah</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formFilterRow2">
                                    <Col sm="6">
                                        <Form.Check
                                            inline
                                            type="radio"
                                            aria-label="Periode"
                                            name="dateOptions"
                                            value="periode"
                                            className="mr-1"
                                            onChange={this.radioButtonChangeHandler}
                                        />
                                        Periode
                                        <Form.Group
                                            as={Row}
                                            controlId="formPeriode"
                                            className="mt-2"
                                        >
                                            <Col sm="11" className="ml-auto">
                                                <Form.Control
                                                    as="select"
                                                    onChange={this.periodeChangeHandler}
                                                    name="periode"
                                                    type="text"
                                                    value={this.state.periode}
                                                >
                                                    {renderPeriodeOptions}
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                    <Col sm="6">
                                        <Form.Group>
                                            <Form.Label>Status Stock Opname</Form.Label>
                                            <Form.Control
                                                as="select"
                                                onChange={this.searchKeyChangeHandler}
                                                name="searchKey"
                                                type="text"
                                                value={this.state.searchKey}
                                            >
                                                <option selected>-- Pilih Penyaring --</option>
                                                <option value="0">Kategori</option>
                                                <option value="1">Daerah</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Verifikasi SO</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <h5>Stock Opname</h5>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <Form inline>
                                    <Form.Control
                                        name="namaDokumen"
                                        type="text"
                                        placeholder="-- Nama Dokumen --"
                                    />
                                    <Form.Control
                                        name="namaDepo"
                                        type="text"
                                        placeholder="-- Nama Depo --"
                                        className="ml-3"
                                    />

                                    <Button inline variant="primary" type="submit" className="ml-3">
                                        Cari
                                    </Button>

                                    <Button
                                        inline
                                        variant="outline-primary"
                                        onClick={this.filterButtonClickHandler}
                                        className="ml-auto"
                                    >
                                        Filter
                                    </Button>
                                </Form>

                                <br></br>
                                <h5> Semua Stock Opname</h5>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Dokumen</th>
                                            <th>Depo</th>
                                            <th>Daerah</th>
                                            <th>Status</th>
                                            <th>Waktu Upload SO</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{rows}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default VerifikasiSO;
