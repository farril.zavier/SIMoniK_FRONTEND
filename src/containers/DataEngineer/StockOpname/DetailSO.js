import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import { api } from "../../../api";

import classes from "./DetailSO.module.css";

class DetailSO extends Component {
    constructor(props) {
        super(props);

        this.state = {
            idStockOpname: props.match.params.id,
            stockOpname: {},
        };
    }

    componentWillMount() {
        this.loadStockOpname();
    }

    loadStockOpname = async () => {
        let idStockOpname = this.state.idStockOpname;
        const response = await api.get("stockopname/stock-opname/" + idStockOpname + "/");
        const stockOpname = response.data;
        this.setState({
            stockOpname: stockOpname,
        });
    };

    render() {
        let namaDepo, namaPemilik, nikPemilik, alamatLengkap, noHp;
        if (this.state.stockOpname.depo !== undefined) {
            namaDepo = this.state.stockOpname.depo.nama;
            namaPemilik = this.state.stockOpname.depo.nama_pemilik;
            nikPemilik = this.state.stockOpname.depo.nik_pemilik;
            alamatLengkap = this.state.stockOpname.depo.alamat_lengkap;
            noHp = this.state.stockOpname.depo.no_hp;
        }
        let renderNewProductTable, renderNewProducts, renderProductTable, renderProducts;
        if (this.state.stockOpname.transactions !== undefined) {
            for (let i = 0; i < this.state.stockOpname.transactions.length; i++) {
                if (this.state.stockOpname.transactions[i].nama_obat.includes("*")) {
                    renderNewProducts = this.state.stockOpname.transactions.map((product, idx) => {
                        if (product.nama_obat.includes("*")) {
                            return (
                                <tr>
                                    <td>{idx + 1}</td>
                                    <td>{this.state.stockOpname.tgl_selesai}</td>
                                    <td>{product.nama_obat}</td>
                                    <td>{product.ukuran_obat}</td>
                                    <td>{product.jumlah_obat}</td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.harga_beli)}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.harga_jual)}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.margin)}
                                    </td>
                                    <td>
                                        {Number(product.percent_margin).toLocaleString("en", {
                                            style: "percent",
                                            minimumFractionDigits: 2,
                                        })}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.cogs_inventory)}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.projected_revenue)}
                                    </td>
                                </tr>
                            );
                        }
                    });
                    renderNewProductTable = (
                        <React.Fragment>
                            <Card.Title>Produk Baru</Card.Title>
                            <Table responsive bordered hover>
                                <thead className="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>Nama Obat</th>
                                        <th>Volume Obat</th>
                                        <th>Jumlah Obat</th>
                                        <th>Harga Beli Satuan</th>
                                        <th>Harga Jual Satuan</th>
                                        <th>Margin Satuan</th>
                                        <th>% Margin</th>
                                        <th>COGS/Inventory</th>
                                        <th>Projected Revenue</th>
                                    </tr>
                                </thead>
                                <tbody>{renderNewProducts}</tbody>
                            </Table>
                        </React.Fragment>
                    );
                    break;
                }
            }
            for (let i = 0; i < this.state.stockOpname.transactions.length; i++) {
                if (!this.state.stockOpname.transactions[i].nama_obat.includes("*")) {
                    renderProducts = this.state.stockOpname.transactions.map((product, idx) => {
                        if (!product.nama_obat.includes("*")) {
                            return (
                                <tr>
                                    <td>{idx + 1}</td>
                                    <td>{this.state.stockOpname.tgl_selesai}</td>
                                    <td>{product.nama_obat}</td>
                                    <td>{product.ukuran_obat}</td>
                                    <td>{product.jumlah_obat}</td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.harga_beli)}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.harga_jual)}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.margin)}
                                    </td>
                                    <td>
                                        {Number(product.percent_margin).toLocaleString("en", {
                                            style: "percent",
                                            minimumFractionDigits: 2,
                                        })}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.cogs_inventory)}
                                    </td>
                                    <td>
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(product.projected_revenue)}
                                    </td>
                                </tr>
                            );
                        }
                    });
                    renderProductTable = (
                        <React.Fragment>
                            <Card.Title>Produk</Card.Title>
                            <Table responsive bordered hover size="sm">
                                <thead className="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>Nama Obat</th>
                                        <th>Volume Obat</th>
                                        <th>Jumlah Obat</th>
                                        <th>Harga Beli Satuan</th>
                                        <th>Harga Jual Satuan</th>
                                        <th>Margin Satuan</th>
                                        <th>% Margin</th>
                                        <th>COGS/Inventory</th>
                                        <th>Projected Revenue</th>
                                    </tr>
                                </thead>
                                <tbody>{renderProducts}</tbody>
                            </Table>
                        </React.Fragment>
                    );
                    break;
                }
            }
        }
        return (
            <React.Fragment>
                <Content>
                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item href="/verifikasi-so">Verifikasi SO</Breadcrumb.Item>
                        <Breadcrumb.Item active>Stock Opname</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title className="d-flex justify-content-between">
                                <span>
                                    {"Stock Opname " +
                                        this.state.stockOpname.no_so +
                                        " " +
                                        namaDepo}
                                </span>
                                <span>
                                    Status:
                                    {this.state.stockOpname.status === 1 &&
                                    this.state.stockOpname !== null ? (
                                        <span style={{ color: "green" }}> Terverifikasi</span>
                                    ) : (
                                        <span style={{ color: "red" }}> Belum Terverifikasi</span>
                                    )}
                                </span>
                            </Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Nama Kios
                                    </Col>
                                    <Col sm="2">: {namaDepo}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        No. HP Pemilik
                                    </Col>
                                    <Col sm="2">: {noHp}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Tgl. Mulai Stock Opname
                                    </Col>
                                    <Col sm="2">: {this.state.stockOpname.tgl_mulai}</Col>
                                </Row>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Nama Pemilik
                                    </Col>
                                    <Col sm="2">: {namaPemilik}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Nama FO Akuisisi
                                    </Col>
                                    <Col sm="2">: {this.state.stockOpname.nama_fo_akuisisi}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Tgl. Selesai Stock Opname
                                    </Col>
                                    <Col sm="2">: {this.state.stockOpname.tgl_selesai}</Col>
                                </Row>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        NIK Pemilik
                                    </Col>
                                    <Col sm="2">: {nikPemilik}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Nama FO Transformasi
                                    </Col>
                                    <Col sm="2">
                                        : {this.state.stockOpname.nama_fo_transformasi}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Total Waktu Stock Opname
                                    </Col>
                                    <Col sm="2">: {this.state.stockOpname.total_waktu} jam</Col>
                                </Row>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Alamat Kios
                                    </Col>
                                    <Col sm="2">: {alamatLengkap}</Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Nama FO Asistensi
                                    </Col>
                                    <Col sm="2">: {this.state.stockOpname.nama_fo_asistensi}</Col>
                                    <Col sm="2"></Col>
                                    <Col sm="2"></Col>
                                </Row>
                                <div className={classes.divider}></div>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Total Jenis Produk
                                    </Col>
                                    <Col sm="2" className="text-danger">
                                        : {this.state.stockOpname.total_jenis_produk}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Inventory Value (COGS)
                                    </Col>
                                    <Col sm="2" className="text-danger">
                                        :{" "}
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(this.state.stockOpname.cogs)}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Projected Gross Profit
                                    </Col>
                                    <Col sm="2" className="text-danger">
                                        :{" "}
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(this.state.stockOpname.projected_gross_profit)}
                                    </Col>
                                </Row>
                                <Row className="mb-2">
                                    <Col sm="2" className="font-weight-bold">
                                        Total SKU
                                    </Col>
                                    <Col sm="2" className="text-danger">
                                        : {this.state.stockOpname.total_sku}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Projected Revenue
                                    </Col>
                                    <Col sm="2" className="text-danger">
                                        :{" "}
                                        {new Intl.NumberFormat("id-ID", {
                                            style: "currency",
                                            currency: "IDR",
                                        }).format(this.state.stockOpname.projected_revenue)}
                                    </Col>
                                    <Col sm="2" className="font-weight-bold">
                                        Projected Gross Margin
                                    </Col>
                                    <Col sm="2" className="text-danger">
                                        :{" "}
                                        {Number(
                                            this.state.stockOpname.projected_gross_margin
                                        ).toLocaleString("en", {
                                            style: "percent",
                                            minimumFractionDigits: 2,
                                        })}
                                    </Col>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Card>

                    <Card className="mt-4">
                        <Card.Body>
                            <Card.Title className="d-flex justify-content-end my-0">
                                {this.state.stockOpname.status === 0 ||
                                this.state.stockOpname.status === null ? (
                                    <React.Fragment>
                                        <Button className="mr-2" variant="primary">
                                            Ubah Data
                                        </Button>
                                        <Button className="ml-2" variant="success">
                                            Verifikasi SO
                                        </Button>
                                    </React.Fragment>
                                ) : (
                                    <React.Fragment></React.Fragment>
                                )}
                            </Card.Title>
                            <Card.Text>
                                {renderNewProductTable}
                                {renderNewProductTable !== undefined &&
                                renderProductTable !== undefined ? (
                                    <br />
                                ) : (
                                    <React.Fragment></React.Fragment>
                                )}
                                {renderProductTable}
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DetailSO;
