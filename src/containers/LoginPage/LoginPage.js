import React, { Component } from "react";
import LoginForm from "./LoginForm/LoginForm";
import Card from "react-bootstrap/Card";

import classes from "./LoginPage.module.css";

class LoginPage extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <div className={classes.loginPage}>
                <Card className={classes.loginCard}>
                    <Card.Body className="p-5">
                        <div className="mb-4">
                            <div className="d-flex justify-content-center">
                                <img
                                    className="mt-n2 mb-2"
                                    src={require("../../assets/simonik.svg")}
                                />
                            </div>
                            <div className={classes.text}>
                                <h6>Sistem Informasi Monitoring</h6>
                                <h6>Kinerja Hara Depo</h6>
                            </div>
                        </div>
                        <LoginForm />
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

export default LoginPage;
