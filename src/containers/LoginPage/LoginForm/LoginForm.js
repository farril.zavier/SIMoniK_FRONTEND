import React, { Component } from "react";
import { connect } from "react-redux";
import { LoginActions } from "../../../actions";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

import classes from "./LoginForm.module.css";

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        };
    }

    changeHandler = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    };

    loginSubmitHandler = async event => {
        const { dispatch } = this.props;
        event.preventDefault();
        const { username, password } = this.state;
        if (username !== "" && password !== "") {
            const response = await dispatch(LoginActions.login(username, password));
            if (response !== undefined) {
                console.log("masuk if");
                if (response.status === 500) {
                    console.log("500");
                }
                if (response.status === 200) {
                    console.log("200");
                    window.location.reload();
                } else {
                    console.log(response.status);
                }
            }
        }
    };

    render() {
        const { loggingIn, messages } = this.props;
        const { username, password } = this.state;
        let loginStatus = "";
        if (!loggingIn && loggingIn !== undefined) {
            loginStatus = <h6 className="text-danger">{messages}</h6>;
        }
        return (
            <div className={classes.loginForm}>
                {loginStatus}
                <Form
                    onSubmit={this.loginSubmitHandler}
                    className="d-flex flex-column justify-content-center"
                >
                    <Form.Group controlId="formUsername">
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                            name="username"
                            type="text"
                            placeholder="Masukkan Username"
                            value={username}
                            onChange={this.changeHandler}
                        />
                    </Form.Group>

                    <Form.Group controlId="formPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            name="password"
                            type="password"
                            placeholder="Masukkan Password"
                            value={password}
                            onChange={this.changeHandler}
                        />
                    </Form.Group>

                    <Button variant="success" className="mt-3 mx-auto" type="submit">
                        Masuk
                    </Button>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { loggingIn, messages } = state.login;
    return {
        loggingIn,
        messages
    };
};

export default connect(mapStateToProps)(LoginForm);
