import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Modal from "react-bootstrap/Modal";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import { api } from "../../../api";

import classes from "./BuatAkun.module.css";

class BuatAkun extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            successModalShow: false,
            failModalShow: false,
            roles: [],
            managers: [],
            username: "",
            name: "",
            role: "",
            password: "",
            noHp: "",
            jenisKelamin: "",
            tglLahir: "",
            tempatLahir: "",
            umur: "",
            manajer: "",
        };
    }

    componentDidMount() {
        this.loadRoles();
        this.loadManajer();
    }

    loadRoles = async () => {
        const fetchedRoles = [];
        const response = await api.get("roles/");
        for (let key in response.data) {
            fetchedRoles.push({
                ...response.data[key],
            });
        }
        this.setState({
            roles: fetchedRoles,
        });
    };

    loadManajer = async () => {
        const fetchedManajer = [];
        const response = await api.get("manajerFieldOfficer/");
        for (let key in response.data) {
            fetchedManajer.push({
                ...response.data[key],
            });
        }
        this.setState({
            managers: fetchedManajer,
        });
    };

    successModalCloseHandler = () => {
        this.setState(
            {
                successModalShow: false,
            },
            () => {
                window.location.reload();
            }
        );
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    handleSubmit = () => {
        if (this.state.role === "Field Officer") {
            this.addFieldOfficer();
        } else if (this.state.role === "Manajer Field Officer") {
            this.addManagerFO();
        } else {
            this.addAccount();
        }
    };

    async addFieldOfficer() {
        let roleId;
        for (let role of this.state.roles) {
            if (this.state.role === role.nama) {
                roleId = role.id;
            }
        }
        const obj = {
            username: this.state.username,
            password: this.state.password,
            role: roleId,
            name: this.state.name,
            umur: this.state.umur,
            no_hp: this.state.noHp,
            tgl_lahir: this.state.tglLahir,
            tempat_lahir: this.state.tempatLahir,
            jenis_kelamin: this.state.jenisKelamin === "Perempuan" ? "0" : "1",
            manajer: this.state.manajer,
        };
        try {
            const response = await api.post("fieldOfficer/", obj);
            if (!response.status === 201) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    async addManagerFO() {
        let roleId;
        for (let role of this.state.roles) {
            if (this.state.role === role.nama) {
                roleId = role.id;
            }
        }
        const account = {
            username: this.state.username,
            password: this.state.password,
            role: roleId,
            name: this.state.name,
        };
        try {
            const response = await api.post("postManajerFieldOfficer/", account);
            if (!response.status === 201) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    async addAccount() {
        let roleId;
        for (let role of this.state.roles) {
            if (this.state.role === role.nama) {
                roleId = role.id;
            }
        }
        const account = {
            username: this.state.username,
            password: this.state.password,
            role: roleId,
            name: this.state.name,
        };
        try {
            const response = await api.post("accounts/", account);
            console.log(account);
            if (!response.status === 201) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({
                successModalShow: true,
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    }

    changeHandler = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    };

    submitForm = (event) => {
        event.preventDefault();
        const { username, password } = this.state;

        const passwordTest = /^(?=.*[A-Za-z])(?=.*d)[A-Za-zd]{8,}$/;
        const usernameTest = /^[0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;

        const passwordCheck = passwordTest.test(password);
        const usernameCheck = usernameTest.test(username);

        console.log(passwordCheck);
        console.log(usernameCheck);
        if (!usernameCheck && !passwordCheck) {
            this.handleSubmit();
        } else {
            this.setState({
                failModalShow: true,
            });
        }
    };

    render() {
        const renderRoles = this.state.roles.map((role, idx) => {
            return (
                <option key={idx} value={role.nama}>
                    {role.nama}
                </option>
            );
        });
        const renderManajer = this.state.managers.map((manajer, idx) => {
            return (
                <option key={idx} value={manajer.id}>
                    {manajer.user.name}
                </option>
            );
        });
        const formFieldOfficer = (
            <div>
                <Form.Group as={Row} controlId="formNomorHp">
                    <Form.Label column sm="2">
                        No Hp*:
                    </Form.Label>
                    <Col sm="4">
                        <Form.Control
                            onChange={this.changeHandler}
                            name="noHp"
                            type="text"
                            pattern="[0-9]*"
                            placeholder="e.g 081234567856"
                        />
                    </Col>
                    <Form.Label column sm="2">
                        Jenis Kelamin*:
                    </Form.Label>
                    <Col sm="4">
                        <Form.Control
                            onChange={this.changeHandler}
                            name="jenisKelamin"
                            as="select"
                            value={this.state.jenisKelamin}
                        >
                            <option selected>-- Pilih Jenis Kelamin --</option>
                            <option value="0">Perempuan</option>
                            <option value="1">Laki-Laki</option>
                        </Form.Control>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formUmur">
                    <Form.Label column sm="2">
                        Umur*:
                    </Form.Label>
                    <Col sm="4">
                        <Form.Control
                            onChange={this.changeHandler}
                            name="umur"
                            type="text"
                            pattern="[0-9]*"
                            placeholder=""
                        />
                    </Col>
                    <Form.Label column sm="2">
                        tglLahir*:
                    </Form.Label>
                    <Col sm="4">
                        <Form.Control
                            onChange={this.changeHandler}
                            name="tglLahir"
                            type="date"
                        ></Form.Control>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formLahir">
                    <Form.Label column sm="2">
                        tempatLahir*:
                    </Form.Label>
                    <Col sm="4">
                        <Form.Control
                            onChange={this.changeHandler}
                            name="tempatLahir"
                            type="text"
                            placeholder=""
                        ></Form.Control>
                    </Col>
                    <Form.Label column sm="2">
                        Manajer Field Officer*:
                    </Form.Label>
                    <Col sm="4">
                        <Form.Control onChange={this.changeHandler} name="manajer" as="select">
                            <option selected>-- Pilih Manajer Field Officer --</option>
                            {renderManajer}
                        </Form.Control>
                    </Col>
                </Form.Group>
            </div>
        );
        return (
            <React.Fragment>
                <Content>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Akun berhasil dibuat!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Akun gagal dibuat!
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Buat Akun</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Akun Baru</Card.Title>
                            <div className={classes.divider}></div>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group as={Row} controlId="formUsername">
                                    <Form.Label column sm="2">
                                        Username*:
                                    </Form.Label>
                                    <Col sm="4">
                                        <Form.Control
                                            onChange={this.changeHandler}
                                            name="username"
                                            type="text"
                                            placeholder="e.g adindahawari"
                                        />
                                    </Col>
                                    <Form.Label column sm="2">
                                        Nama*:
                                    </Form.Label>
                                    <Col sm="4">
                                        <Form.Control
                                            onChange={this.changeHandler}
                                            name="name"
                                            type="text"
                                            placeholder="e.g Adinda Hawari"
                                        />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} controlId="formUsername">
                                    <Form.Label column sm="2">
                                        Password*:
                                    </Form.Label>
                                    <Col sm="4">
                                        <Form.Control
                                            onChange={this.changeHandler}
                                            name="password"
                                            type="password"
                                            placeholder="********"
                                        />
                                    </Col>
                                    <Form.Label column sm="2">
                                        Role*:
                                    </Form.Label>
                                    <Col sm="4">
                                        <Form.Control
                                            onChange={this.changeHandler}
                                            name="role"
                                            as="select"
                                        >
                                            <option selected>-- Pilih Role --</option>
                                            {renderRoles}
                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                                {this.state.role === "Field Officer" && formFieldOfficer}
                                <Row>
                                    <Col sm="2"></Col>
                                    <Col sm="10">
                                        <Button variant="primary" onClick={this.submitForm}>
                                            Buat Akun
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default BuatAkun;
