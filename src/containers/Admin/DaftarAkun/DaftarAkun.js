import React, { Component } from "react";
import Content from "../../../components/Content/Content";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import { api } from "../../../api";
import Akun from "./Akun/Akun";

import classes from "./DaftarAkun.module.css";

class DaftarAkun extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalShow: false,
            successModalShow: false,
            failModalShow: false,
            accounts: [],
            currentId: undefined,
            roles: [],
            filter: 0,
            role: 0,
        };
    }

    modalShowHandler(accountId) {
        this.setState({
            modalShow: true,
            currentId: accountId,
        });
    }

    modalCloseHandler = () => {
        this.setState({
            modalShow: false,
            currentId: undefined,
        });
    };

    componentDidMount() {
        this.loadAccounts();
        this.loadRoles();
    }

    loadAccounts = async () => {
        const fetchedAccounts = [];
        const response = await api.get("accounts/");
        for (let key in response.data) {
            fetchedAccounts.push({
                ...response.data[key],
            });
        }
        this.setState({
            accounts: fetchedAccounts,
        });
    };

    loadRoles = async () => {
        const fetchedRoles = [];
        const response = await api.get("roles/");
        for (let key in response.data) {
            fetchedRoles.push({
                ...response.data[key],
            });
        }
        this.setState({
            roles: fetchedRoles,
        });
    };

    deleteAccount = async () => {
        const accountId = this.state.currentId;
        this.modalCloseHandler();
        try {
            const response = await api.del("accounts/" + accountId + "/");
            if (!response.status === 200 || !response.status === 204) {
                this.setState(
                    {
                        failModalShow: true,
                    },
                    () => {
                        throw Error(response.statusText);
                    }
                );
            }
            this.setState({ successModalShow: true }, async () => {
                await this.loadAccounts();
            });
        } catch (error) {
            console.log(error);
            this.setState({
                failModalShow: true,
            });
        }
    };

    successModalCloseHandler = () => {
        this.setState({
            successModalShow: false,
        });
    };

    failModalCloseHandler = () => {
        this.setState({
            failModalShow: false,
        });
    };

    filterHandler = (event) => {
        const { name, value } = event.target;
        console.log(name + value);
        this.setState({
            [name]: value,
        });
    };

    submitHandler = (event) => {
        event.preventDefault();
        this.setState({ filter: this.state.role });
    };

    render() {
        const renderAccounts = this.state.accounts
            .filter((account, idx) => this.state.filter == 0 || account.role == this.state.filter)
            .map((account, idx) => (
                <Akun
                    key={idx}
                    idx={idx + 1}
                    name={account.name}
                    role={account.role_name}
                    username={account.username}
                    delete={() => this.modalShowHandler(account.id)}
                />
            ));
        const renderRoles = this.state.roles.map((role, idx) => {
            return (
                <option key={idx} value={role.id}>
                    {role.nama}
                </option>
            );
        });
        return (
            <React.Fragment>
                <Content>
                    <Modal centered show={this.state.modalShow} onHide={this.modalCloseHandler}>
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Apakah Anda yakin ingin menghapus akun ini?
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                <Button onClick={this.deleteAccount} variant="primary">
                                    Ya, Hapus
                                </Button>
                                <Button onClick={this.modalCloseHandler} variant="light">
                                    Tidak
                                </Button>
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.successModalShow}
                        onHide={this.successModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/check_circle.svg")} />
                            </Row>
                            <Row className="justify-content-center text-center mb-3 mt-4">
                                Akun berhasil dihapus!
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal
                        centered
                        show={this.state.failModalShow}
                        onHide={this.failModalCloseHandler}
                    >
                        <Modal.Header closeButton className={classes.modalHeader}></Modal.Header>
                        <Modal.Body className="mt-n5">
                            <Row className="justify-content-center mt-2 mb-4">
                                <img src={require("../../../assets/x_circle.svg")} />
                            </Row>
                            <Row className="justify-content center text-center mb-3 mt-4">
                                Akun gagal dihapus!
                            </Row>
                        </Modal.Body>
                    </Modal>

                    <Breadcrumb className={classes.breadcrumbBackground}>
                        <Breadcrumb.Item active>Daftar Akun</Breadcrumb.Item>
                    </Breadcrumb>

                    <Card>
                        <Card.Body>
                            <Card.Title>Akun</Card.Title>
                            <div className={classes.divider}></div>
                            <Card.Text>
                                <p> Cari Berdasarkan: </p>
                                <Form inline onSubmit={this.submitHandler}>
                                    <Form.Control
                                        onChange={this.filterHandler}
                                        name="role"
                                        as="select"
                                    >
                                        <option selected value={0}>
                                            -- Semua Role --
                                        </option>
                                        {renderRoles}
                                    </Form.Control>
                                    <Button variant="primary" type="submit">
                                        Cari
                                    </Button>
                                </Form>

                                <br></br>
                                <p> Semua Akun </p>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Role</th>
                                            <th>Username</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>{renderAccounts}</tbody>
                                </Table>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Content>
            </React.Fragment>
        );
    }
}

export default DaftarAkun;
