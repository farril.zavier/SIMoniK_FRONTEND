import React from "react";
import Button from "react-bootstrap/Button";

import classes from "./Akun.module.css";

const Akun = props => {
    return (
        <tr>
            <td>{props.idx}</td>
            <td>{props.name}</td>
            <td>{props.role}</td>
            <td>{props.username}</td>
            <td>
                <Button onClick={props.delete} variant="danger">
                    Hapus
                </Button>
            </td>
        </tr>
    );
};

export default Akun;
