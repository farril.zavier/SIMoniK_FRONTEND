import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Home from "./containers/Home/Home";
import LoginPage from "./containers/LoginPage/LoginPage";
import UnggahSO from "./containers/FieldOfficer/UnggahSO/UnggahSO";
import DaftarDeadline from "./containers/FieldOfficer/DaftarDeadline/DaftarDeadline";
import DaftarTugas from "./containers/FieldOfficer/DaftarTugas/DaftarTugas";
import DaftarAkun from "./containers/Admin/DaftarAkun/DaftarAkun";
import BuatAkun from "./containers/Admin/BuatAkun/BuatAkun";
import VerifikasiSO from "./containers/DataEngineer/StockOpname/VerifikasiSO";
import DetailSO from "./containers/DataEngineer/StockOpname/DetailSO";
import DaftarProduk from "./containers/DataEngineer/Produk/DaftarProduk";
import TambahProduk from "./containers/DataEngineer/Produk/TambahProduk";
import DetailProduk from "./containers/DataEngineer/Produk/DetailProduk";
import EditProduk from "./containers/DataEngineer/Produk/EditProduk";
import DaftarFieldOfficer from "./containers/ManagerFO/FieldOfficer/DaftarFieldOfficer";
import JadwalPenugasan from "./containers/ManagerFO/FieldOfficer/JadwalPenugasan";
import EditPenugasan from "./containers/ManagerFO/FieldOfficer/EditPenugasan";
import DaftarPenugasan from "./containers/ManagerFO/FieldOfficer/DaftarPenugasan";
import TambahDepo from "./containers/DataEngineer/Depo/TambahDepo";
import EditDepo from "./containers/DataEngineer/Depo/EditDepo";
import DetailDepo from "./containers/DataEngineer/Depo/DetailDepo";
import DaftarDepo from "./containers/DataEngineer/Depo/DaftarDepo";
import Keuntungan from "./containers/BusinessDevelopment/Keuntungan/Keuntungan";
import DetailKeuntunganProduk from "./containers/BusinessDevelopment/Keuntungan/DetailProduk/DetailProduk";
import PenjualanProduk from "./containers/BusinessDevelopment/PenjualanProduk/PenjualanProduk";
import DetailPenjualanProduk from "./containers/BusinessDevelopment/PenjualanProduk/DetailProduk/DetailProduk";
import ProdukPopuler from "./containers/BusinessDevelopment/ProdukPopuler/ProdukPopuler";
import DetailProdukPopuler from "./containers/BusinessDevelopment/ProdukPopuler/DetailProduk/DetailProduk";
import DepoBisdev from "./containers/BusinessDevelopment/Depo/Depo";
import RingkasanKinerjaDepo from "./containers/BusinessDevelopment/Depo/RingkasanKinerjaDepo/RingkasanKinerjaDepo";
import PerbandinganProduk from "./containers/BusinessDevelopment/Depo/RingkasanKinerjaDepo/PerbandinganProduk/PerbandinganProduk";

const Routes = (props) => (
    <Switch>
        <Route exact path="/" component={props.home} />
        <Route path="/login" component={LoginPage} />

        {/* Admin */}
        <Route path="/daftar-akun" component={DaftarAkun} />
        <Route path="/buat-akun" component={BuatAkun} />

        {/* Field Officer */}
        <Route path="/deadline/detail/:idDeadlineFo/unggah-so/:idDepo" component={UnggahSO} />
        <Route path="/deadline/detail/:id" component={DaftarTugas} />
        <Route path="/daftar-deadline" component={DaftarDeadline} />

        {/* Manager FO */}
        <Route path="/daftar-fo" component={DaftarFieldOfficer} />
        <Route path="/penugasan/tambah" component={JadwalPenugasan} />
        <Route path="/penugasan/ubah/:id" component={EditPenugasan} />
        <Route path="/penugasan" component={DaftarPenugasan} />

        {/* Data Engineer */}
        <Route path="/verifikasi-so/detail/:id" component={DetailSO} />
        <Route path="/verifikasi-so" component={VerifikasiSO} />
        <Route path="/produk/tambah" component={TambahProduk} />
        <Route path="/produk/detail/:id" component={DetailProduk} />
        <Route path="/produk/ubah/:id" component={EditProduk} />
        <Route path="/produk" component={DaftarProduk} />
        <Route path="/depo/tambah" component={TambahDepo} />
        <Route path="/depo/ubah/:id" component={EditDepo} />
        <Route path="/depo/detail/:id" component={DetailDepo} />
        <Route path="/depo" component={DaftarDepo} />

        {/* Business Development */}
        <Route path="/keuntungan/detail/:id" component={DetailKeuntunganProduk} />
        <Route path="/keuntungan" component={Keuntungan} />
        <Route path="/penjualan-produk/detail/:id" component={DetailPenjualanProduk} />
        <Route path="/penjualan-produk" component={PenjualanProduk} />
        <Route path="/produk-populer/detail/:id" component={DetailProdukPopuler} />
        <Route path="/produk-populer" component={ProdukPopuler} />
        <Route
            path="/daftar-depo/ringkasan-kinerja/perbandingan-produk"
            component={PerbandinganProduk}
        />
        <Route path="/daftar-depo/ringkasan-kinerja/:id" component={RingkasanKinerjaDepo} />
        <Route path="/daftar-depo" component={DepoBisdev} />
    </Switch>
);

export default Routes;
