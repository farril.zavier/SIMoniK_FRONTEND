import React, { Component } from "react";
import { connect } from "react-redux";
import { LoginActions } from "../../actions";

import classes from "./Header.module.css";

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: "",
            role: "",
        };
    }

    componentDidMount() {
        var user = localStorage.getItem("user");
        var role = localStorage.getItem("role");
        this.setState({
            user: user,
            role: role,
        });
    }

    componentWillUnmount() {
        this.setState({
            user: "",
            role: "",
        });
    }

    logout = () => {
        const { dispatch } = this.props;
        dispatch(LoginActions.logout());
        localStorage.clear();
        window.location.reload();
    };

    render() {
        return (
            <div className={classes.header}>
                <div className={classes.headerLeft}>
                    <img className={classes.headerBrand} src={require("../../assets/hara.svg")} />
                </div>
                <div className={classes.headerRight}>
                    <h5 className={classes.role}>{this.state.role}</h5>
                    <div className={classes.headerSeparator}></div>
                    <button onClick={this.logout} className={classes.logOutButton}>
                        Log Out
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { login } = state;
    return {
        login,
    };
};

export default connect(mapStateToProps)(Header);
