import React, { Component } from "react";
import { Link } from "react-router-dom";

import classes from "./Sidebar.module.css";

class Sidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: "",
            role: "",
        };
    }

    componentDidMount() {
        var user = localStorage.getItem("user");
        var role = localStorage.getItem("role");
        this.setState({
            user: user,
            role: role,
        });
    }

    componentWillUnmount() {
        this.setState({
            user: "",
            role: "",
        });
    }

    render() {
        return (
            <div className={classes.sidebar}>
                <div className={classes.brand}>
                    <img src={require("../../assets/simonik.svg")} />
                    <div className={classes.text}>
                        <h6>Sistem Informasi Monitoring</h6>
                        <h6>Kinerja Hara Depo</h6>
                    </div>
                </div>
                <div className={classes.separator}></div>
                <div className={classes.userInfo}>
                    <h5>{this.state.user}</h5>
                    <h5>({this.state.role})</h5>
                </div>
                <div className={classes.separator}></div>
                <div className={classes.links}>
                    {this.props.routes.map((route) => {
                        return (
                            <Link to={route.path} className={classes.link}>
                                <h5>{route.name}</h5>
                                <img
                                    className={classes.linkArrow}
                                    src={require("../../assets/arrow_forward.svg")}
                                />
                            </Link>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default Sidebar;
