import { api } from "../api";
import * as types from "../constants/action-types";

const login = (username, password) => {
    return async (dispatch) => {
        try {
            const apiEndpoint = "token/obtain/";
            const payload = {
                username: username,
                password: password,
            };
            const response = await api.post(apiEndpoint, payload);
            if (response !== undefined) {
                if (response.data !== undefined) {
                    localStorage.setItem("access", response.data.access);
                    localStorage.setItem("refresh", response.data.refresh);
                    localStorage.setItem("user", response.data.user);
                    localStorage.setItem("role", response.data.role);
                    dispatch(auths(types.LOGIN_SUCCESS, response.data));
                    return response;
                } else {
                    dispatch(
                        auths(types.LOGIN_FAILED, "Internal Server Error")
                    );
                }
            } else {
                dispatch(auths(types.LOGIN_FAILED, "Internal Server Error"));
            }
        } catch (error) {
            if (error.status === 401) {
                dispatch(
                    auths(types.LOGIN_FAILED, "Username atau Password salah")
                );
            } else {
                return error;
            }
        }
    };
};

const logout = () => {
    return async () => {
        const apiEndpoint = "logout/";
        const payload = {
            refresh: localStorage.getItem("refresh"),
        };
        const response = await api.post(apiEndpoint, payload);
        return response.status;
    };
};

export const auths = (type, data) => {
    return {
        type: type,
        data: data,
    };
};

export const LoginActions = {
    login,
    logout,
};
