import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import { api } from "./api";
import jwt_decode from "jwt-decode";
import { connect } from "react-redux";
import Router from "./routes";
import { routes } from "./route-menu";
import { withRouter } from "react-router-dom";

import ScrollToTop from "./ScrollToTop";
import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import LoginPage from "./containers/LoginPage/LoginPage";

import DaftarAkun from "./containers/Admin/DaftarAkun/DaftarAkun";
import DaftarDeadline from "./containers/FieldOfficer/DaftarDeadline/DaftarDeadline";
import DaftarFieldOfficer from "./containers/ManagerFO/FieldOfficer/DaftarFieldOfficer";
import VerifikasiSO from "./containers/DataEngineer/StockOpname/VerifikasiSO";
import Dashboard from "./containers/BusinessDevelopment/Dashboard/Dashboard";
import Home from "./containers/Home/Home";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: "",
            role: "",
        };
    }

    componentDidMount() {
        var user = localStorage.getItem("user");
        var role = localStorage.getItem("role");
        this.setState(
            {
                user: user,
                role: role,
            },
            () => {
                const currentUrl = window.location.pathname;
                if (currentUrl !== "/login/") {
                    api.refresh();
                }
            }
        );
    }

    componentWillUnmount() {
        this.setState({
            user: "",
            role: "",
        });
    }

    render() {
        const currentUrl = window.location.pathname;
        let page = null;

        if (localStorage.getItem("access") === null) {
            if (currentUrl === "/login/") {
                page = <LoginPage />;
            } else {
                this.props.history.push("/login/");
            }
        } else {
            const decodedToken = jwt_decode(localStorage.getItem("access"));
            const currentTimestamp = Math.floor(Date.now() / 1000);
            if (decodedToken.exp < currentTimestamp) {
                localStorage.clear();
                if (currentUrl === "/login/") {
                    page = <LoginPage />;
                } else {
                    this.props.history.push("/login/");
                }
            } else {
                if (currentUrl === "/login/") {
                    this.props.history.push("/");
                    window.location = window.location.hostname + "/";
                } else {
                    if (this.state.role === "Admin") {
                        if (
                            currentUrl.includes("daftar-akun") ||
                            currentUrl.includes("buat-akun") ||
                            currentUrl === "/"
                        ) {
                            page = (
                                <React.Fragment>
                                    <ScrollToTop />
                                    <Header />
                                    <Sidebar routes={routes.adminRoutes} />
                                    <Router home={DaftarAkun} />
                                </React.Fragment>
                            );
                        } else {
                            this.props.history.push("/");
                        }
                    } else if (this.state.role === "Field Officer") {
                        if (
                            currentUrl.includes("unggah-so") ||
                            currentUrl.includes("daftar-deadline") ||
                            currentUrl.includes("deadline") ||
                            currentUrl === "/"
                        ) {
                            page = (
                                <React.Fragment>
                                    <ScrollToTop />
                                    <Header />
                                    <Sidebar routes={routes.fieldOfficerRoutes} />
                                    <Router home={DaftarDeadline} />
                                </React.Fragment>
                            );
                        } else {
                            this.props.history.push("/");
                        }
                    } else if (this.state.role === "Manajer Field Officer") {
                        if (
                            currentUrl.includes("daftar-fo") ||
                            currentUrl.includes("penugasan") ||
                            currentUrl === "/"
                        ) {
                            page = (
                                <React.Fragment>
                                    <ScrollToTop />
                                    <Header />
                                    <Sidebar routes={routes.manajerFoRoutes} />
                                    <Router home={DaftarFieldOfficer} />
                                </React.Fragment>
                            );
                        } else {
                            this.props.history.push("/");
                        }
                    } else if (this.state.role === "Data Engineer") {
                        if (
                            currentUrl.includes("rekap-so") ||
                            currentUrl.includes("verifikasi-so") ||
                            currentUrl.includes("produk") ||
                            currentUrl.includes("depo") ||
                            currentUrl === "/"
                        ) {
                            page = (
                                <React.Fragment>
                                    <ScrollToTop />
                                    <Header />
                                    <Sidebar routes={routes.dataEngineerRoutes} />
                                    <Router home={VerifikasiSO} />
                                </React.Fragment>
                            );
                        } else {
                            this.props.history.push("/");
                        }
                    } else if (this.state.role === "Business Development") {
                        if (
                            currentUrl.includes("keuntungan") ||
                            currentUrl.includes("penjualan-produk") ||
                            currentUrl.includes("produk-populer") ||
                            currentUrl.includes("daftar-depo") ||
                            currentUrl === "/"
                        ) {
                            page = (
                                <React.Fragment>
                                    <ScrollToTop />
                                    <Header />
                                    <Sidebar routes={routes.businessDevelopmentRoutes} />
                                    <Router home={Dashboard} />
                                </React.Fragment>
                            );
                        } else {
                            this.props.history.push("/");
                        }
                    }
                }
            }
        }
        return <React.Fragment>{page}</React.Fragment>;
    }
}

const mapStateToProps = (state) => {
    const { login } = state;
    return {
        login,
    };
};

export default connect(mapStateToProps)(withRouter(App));
