import axiosInstance from "./config";

export const api = {
    refresh,
    get,
    post,
    put,
    del,
    postStockOpname,
};

async function refresh() {
    try {
        const refresh_token = localStorage.getItem("refresh");

        return axiosInstance
            .post("token/refresh/", { refresh: refresh_token })
            .then((response) => {
                localStorage.setItem("access", response.data.access);
                localStorage.setItem("refresh", response.data.refresh);

                axiosInstance.defaults.headers["Authorization"] = "JWT " + response.data.access;

                return response;
            })
            .catch((err) => {
                console.log(err);
            });
    } catch (error) {
        return Promise.reject(error.response);
    }
}

async function get(apiEndpoint) {
    try {
        const response = await axiosInstance.get(apiEndpoint, {
            headers: {
                "Content-Type": "application/json",
                Authorization: "JWT " + localStorage.getItem("access"),
            },
        });
        return response;
    } catch (error) {
        return Promise.reject(error.response);
    }
}

async function post(apiEndpoint, payload) {
    try {
        const response = await axiosInstance.post(apiEndpoint, payload);
        return response;
    } catch (error) {
        return Promise.reject(error.response);
    }
}

async function put(apiEndpoint, payload) {
    try {
        const response = await axiosInstance.put(apiEndpoint, payload, {
            headers: {
                "Content-Type": "application/json",
                Authorization: "JWT " + localStorage.getItem("access"),
            },
        });
        return response;
    } catch (error) {
        return Promise.reject(error.response);
    }
}

async function del(apiEndpoint) {
    try {
        const response = await axiosInstance.delete(apiEndpoint, {
            headers: {
                "Content-Type": "application/json",
                Authorization: "JWT " + localStorage.getItem("access"),
            },
        });
        return response;
    } catch (error) {
        return Promise.reject(error.response);
    }
}

async function postStockOpname(formData) {
    try {
        const response = await axiosInstance.post("upload-so/upload-so/", formData, {
            headers: {
                "Content-Type": "multipart/form-data",
                "Content-Disposition": "attachment; filename=" + formData.get("file").name,
                filename: formData.get("file").name,
                Authorization: "JWT " + localStorage.getItem("access"),
            },
        });
        return response;
    } catch (error) {
        return Promise.reject(error.response);
    }
}
