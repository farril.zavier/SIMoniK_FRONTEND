import axios from "axios";

const axiosInstance = axios.create({
    baseURL: "https://propensi-a7-backend.herokuapp.com/api/",
    headers: {
        Authorization: "JWT " + localStorage.getItem("access"),
        "Content-Type": "application/json",
        accept: "application/json",
    },
});

axiosInstance.interceptors.response.use(
    (response) => response,
    (error) => {
        const originalRequest = error.config;

        if (error.response.status === 401 && error.response.statusText === "Unauthorized") {
            const refresh_token = localStorage.getItem("refresh");

            return axiosInstance
                .post("token/refresh/", { refresh: refresh_token })
                .then((response) => {
                    localStorage.setItem("access", response.data.access);
                    localStorage.setItem("refresh", response.data.refresh);

                    axiosInstance.defaults.headers["Authorization"] = "JWT " + response.data.access;
                    originalRequest.headers["Authorization"] = "JWT " + response.data.access;

                    return axiosInstance(originalRequest);
                })
                .catch((err) => {
                    console.log(err);
                });
        }
        return Promise.reject(error);
    }
);

export default axiosInstance;
