var adminRoutes = [
    {
        path: "/daftar-akun",
        name: "Daftar Akun",
    },
    {
        path: "/buat-akun",
        name: "Buat Akun",
    },
];

var fieldOfficerRoutes = [
    {
        path: "/daftar-deadline",
        name: "Daftar Deadline",
    },
];

var manajerFoRoutes = [
    {
        path: "/daftar-fo",
        name: "Daftar Field Officer",
    },
    {
        path: "/penugasan",
        name: "Daftar Penugasan",
    },
    {
        path: "/penugasan/tambah",
        name: "Tambah Penugasan",
    },
];

var dataEngineerRoutes = [
    {
        path: "/rekap-so",
        name: "Rekap SO",
    },
    {
        path: "/verifikasi-so",
        name: "Verifikasi SO",
    },
    {
        path: "/produk",
        name: "Produk",
    },
    {
        path: "/depo",
        name: "Depo",
    },
];

var businessDevelopmentRoutes = [
    {
        path: "/",
        name: "Dashboard",
    },
    {
        path: "/keuntungan",
        name: "Keuntungan",
    },
    {
        path: "/penjualan-produk",
        name: "Penjualan Produk",
    },
    {
        path: "/produk-populer",
        name: "Produk Populer",
    },
    {
        path: "/daftar-depo",
        name: "Depo",
    },
];

export const routes = {
    adminRoutes,
    fieldOfficerRoutes,
    manajerFoRoutes,
    dataEngineerRoutes,
    businessDevelopmentRoutes,
};
